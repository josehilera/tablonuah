# Tablón UAH
Aplicación universitaria en la que se pueden subir y consultar apuntes de universidad, organizados por facultades, grados, cursos, asignaturas y categorías.

**Descargo de responsabilidad**: Esta aplicación no es una aplicación oficial de la Universidad de Alcalá (UAH)

## Pipelines

Utilizar la variable **`CI_COMMIT_MESSAGE`** para usar las opciones al ejecutar nuevos pipelines de forma manual desde _GitLab_.

> No actualizar versionCode ni versionName a mano en el código, sino usar las opciones

### Stages

#### **`test`**
Ejecuta tests unitarios.

#### **`ui-test`**
Ejecuta tests de interfaz de usuario en un emulador.

Se le puede pasar un usuario y contraseña específicos con las opciones `[username]` y `[password]`.

Si se añade la opción `[screengrab]`, se utilizará el comando `fastlane screengrab` para ejecutar los tests, el cual saca capturas de pantalla durante los mismos, y se subirán a la _Google Play Console_ con el comando `fastlane supply`.

#### **`build`**
Primero actualiza el `versionName` y el `versionCode` en `app/build.gradle` si es que así se ha especificado con las opciones `[versionName]` y `[versionCode]`.

Si no se ha especificado un `versionCode` usando su opción correspondiente, y solo si se ha añadido la opción `[release]`, se incrementará automáticamente en 1 respecto a su valor anterior con el comando `fastlane increment_vc`.

Estos cambios, si es que los ha habido, se pushearán al repositorio (con una opción que hará que se omita la ejecución de un nuevo pipeline).

Posteriormente, se construirá la aplicación con el comando `fastlane build` y se guardará como artefacto para la siguiente stage.

#### **`upload_to_play_store`**
Esta stage solo se activará cuando se esté en la `branch` `master` y se añada la opción `[release]`.

Si se ha añadido la opción `[frameit]` y no se ha añadido la opción `[skip-upload-screenshots]`, utilizará el comando `fastlane frameit android` para añadir marco, fondo, títulos y texto a las capturas de pantalla que se encuentren en `fastlane/metadata/android/es-ES/images/phoneScreenshots` según lo descrito en el archivo `Framefile.json`.

Por último, se ejecutará el comando `fastlane upload_app_to_play_store` para subir la aplicación construida en la stage `build` a la _Google Play Console_ en la `track` `internal` (o la especificada con la opción `[track]`) y con el release status `draft` (o `completed` si así se ha especificado mediante la opción `[deploy]`).

A su vez, también se subirán a la _Google Play Console_ las capturas de pantalla que se encuentren en ese momento en `fastlane/metadata/android/es-ES/images/phoneScreenshots` (se haya ejecutado `frameit` o no), si es que no se ha añadido la opción `[skip-upload-screenshots]`.

### Opciones

#### **`[skip-unit-test]`**
- Deshabilita la stage `test`, saltándose así la ejecución de tests unitarios.

#### **`[skip-ui-test]`**
- Deshabilita la stage `ui-test`, saltándose así la ejecución de tests de interfaz de usuario.

#### **`[username]`**
- Establece el usuario utilizado durante los tests de interfaz de usuario con lo que haya detrás de ":".
- Si no hay nada o solo espacios entre ":" y "]", se utilizará `alvlop18` por defecto.

#### **`[password]`**
- Establece la contraseña utilizada durante los tests de interfaz de usuario con lo que haya detrás de ":".
- Si no hay nada o solo espacios entre ":" y "]", se utilizará `testGoogle123` por defecto.

#### **`[screengrab]`**
- Hará que durante la stage `ui-test` (si es que no se ha deshabilitado con la opción `[skip-ui-test]`), se utilice el comando `fastlane screengrab` para ejecutar los tests, el cual saca capturas de pantalla durante los mismos, y se subirán a la _Google Play Console_ con el comando `fastlane supply`.
- Con las opciones _`[track]`_ y _`[supplyVersionCode]`_  podremos especificar sobre qué `track` y en qué `versionCode` se subirán las screenshots al ejecutar el comando `fastlane screengrab`.
    - Sus valores por defecto al no usar las opciones, serán `internal` y el `versionCode` alojado en `app/build.gradle` respectivamente.
    - Es necesario que ya exista una release con el `versionCode` especificado en la `track` especificada para que no se genere un error.

#### **`[supplyVersionCode:50]`**
- Establece el `versionCode` que se utilizará en el comando `fastlane supply` durante la stage `ui-test` para subir las capturas a la _Google Play Console_ (si es que además se ha utilizado la opción `[screengrab]` y no se ha utilizado la opción `[skip-ui-test]`).
- Escribir un número entero.
- Si no hay nada o solo espacios entre ":" y "]", lo ignora y no cambia el `versionCode`.

De forma predeterminada se utiliza el `versionCode` que hay en el código (en `app/build.gradle`) a la hora de ejecutarse el pipeline, pero si por alguna razón es necesario utilizar un `versionCode` distinto al del código, se puede usar esta opción.

Se posibilita esta opción para evitar errores, ya que debe existir en la _Google Play Console_ una release para la `track` seleccionada (`internal` de forma predeterminada o la establecida con la opción _`[track]`_) y con el `versionCode` que se haya establecido al usar el comando `fastlane supply`, ya sea el suministrado por esta opción (si se utiliza) o el del código (si no se utiliza).

#### **`[versionName:1.0.0]`**
- Establece el versionName de la aplicación en `app/build.gradle` con lo que haya detrás de ":".
- Este nuevo nombre se pusheará al repositorio y será el que se utilice en la stage `build` para que posteriormente se suba a la _Google Play Console_.
- Seguir el formato de string `<major>.<minor>.<point>`.
- Si no hay nada o solo espacios entre ":" y "]", lo ignora y no cambia el versionName.
#### **`[versionCode:1]`**
- Establece el `versionCode` de la aplicación en `app/build.gradle` con lo que haya detrás de ":".
- Este nuevo `versionCode` se pusheará al repositorio y será el que se utilice en la stage `build` para que posteriormente se suba a la _Google Play Console_.
- Escribir un número entero.
- Si no hay nada o solo espacios entre ":" y "]", lo ignora y no cambia el `versionCode`.

Al pushear el cambio, nos aseguramos de que, cuando se actualice el `versionCode` en el código del repositorio (siempre y cuando luego no haya errores en el `build` o en la subida a la _Google Play Console_), se suba y así exista una release con dicho `versionCode`, evitando así los errores mencionados al ejecutar el comando `fastlane supply` en los pipelines siguientes (aunque siempre se puede evitar este error especificando uno concreto con la opción _`[supplyVersionCode]`_).

#### **`[frameit]`**
- Si no se ha añadido la opción `[skip-upload-screenshots]`, utilizará el comando `fastlane frameit android` para añadir marco, fondo, títulos y texto a las capturas de pantalla que se encuentren en `fastlane/metadata/android/es-ES/images/phoneScreenshots` según lo descrito en el archivo `Framefile.json` durante la stage `upload_to_play_store`.

#### **`[skip-upload-screenshots]`**
- Omitirá la subida de las fotos ubicadas en `fastlane/metadata/android/es-ES/images/phoneScreenshots` durante la ejecución del comando `fastlane upload_app_to_play_store` en la stage `upload_to_play_store`.

#### **`[release]`**
- Habilita la stage `upload_to_play_store` para subir una nueva versión a la _Google Play Console_.

#### **`[track:production]`**
- Establece la `track` en la que se subirá la nueva release a la _Google Play Console_ y sobre la que se subirán las screenshots de `screengrab` si es que procede.
- Las opciones son: _`internal`_, _`alpha`_, _`beta`_ y _`production`_.
- Si no se utiliza la opción o si no hay nada o solo espacios entre ":" y "]", se utiliza la `track` _`internal`_ por defecto.

#### **`[deploy]`**
- Establece el release status como `completed` al subir una nueva release a la _Google Play Console_.
- Esto hará que la release sea enviada directamente a revisión y desplegada en producción una vez dicha revisión sea favorable.
- Si no se incluye esta opción, el release status por defecto será `draft`, lo que dejará la release en estado de borrador, necesitando un envío manual para su revisión desde la _Google Play Console_.

### Ejemplos

> App development [versionName:2.0.0][track:production][release][frameit]

Ejecución de tests y subida a producción de la versión 2.0.0 subiendo las capturas de pantalla ubicadadas en `fastlane/metadata/android/es-ES/images/phoneScreenshots` decorándolas antes el comando `fastlane frameit`.
