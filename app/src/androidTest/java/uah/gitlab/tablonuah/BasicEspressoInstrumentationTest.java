package uah.gitlab.tablonuah;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import tools.fastlane.screengrab.Screengrab;
import tools.fastlane.screengrab.UiAutomatorScreenshotStrategy;
import tools.fastlane.screengrab.cleanstatusbar.BluetoothState;
import tools.fastlane.screengrab.cleanstatusbar.CleanStatusBar;
import tools.fastlane.screengrab.cleanstatusbar.MobileDataType;
import tools.fastlane.screengrab.locale.LocaleTestRule;
import uah.gitlab.tablonuah.ui.LogIn;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

@RunWith(JUnit4.class)
public class BasicEspressoInstrumentationTest {

    private static final String USERNAME_VARIABLE_NAME = "username";
    private static final String PASSWORD_VARIABLE_NAME = "password";

    private static final String LOCAL_TESTS_USERNAME = "alvlop20";
    private static final String LOCAL_TESTS_PASSWORD = "1234";

    @ClassRule
    public static final LocaleTestRule localeTestRule = new LocaleTestRule();

    @Rule
    public ActivityScenarioRule<LogIn> activityRule = new ActivityScenarioRule<>(LogIn.class);

    @BeforeClass
    public static void beforeAll() {
        new CleanStatusBar()
                .setMobileNetworkDataType(MobileDataType.HIDE)
                .setBatteryLevel(70)
                .setMobileNetworkLevel(3)
                .setWifiLevel(4)
                .setBluetoothState(BluetoothState.DISCONNECTED)
                .enable();

        InstrumentationRegistry.getInstrumentation().getContext()
                .sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        InstrumentationRegistry.getInstrumentation().getTargetContext()
                .sendBroadcast(new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        UiObject closeAppButton = device.findObject(new UiSelector().textContains("Close app"));
        if (closeAppButton.exists()) {
            try {
                closeAppButton.click();
            } catch (UiObjectNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @AfterClass
    public static void afterAll() {
        CleanStatusBar.disable();
    }

    private String getExtra(String variable) {
        Bundle extras = InstrumentationRegistry.getArguments();
        if (extras != null) {
            if (extras.containsKey(variable)) {
                String result = extras.getString(variable);
                System.out.println("Variable " + variable + " found in extras: " + result);
                return result;
            } else {
                System.out.println("No variable " + variable + " found in extras");
                return null;
            }
        } else {
            System.out.println("No extras");
            return null;
        }
    }

    private void printVariableUsed(String value, String variableName) {
        System.out.println("Using " + value + " as " + variableName);
    }

    @Test
    public void testLogIn() throws InterruptedException {
        String nombreUsuarioExtra = getExtra(USERNAME_VARIABLE_NAME);
        String passwordExtra = getExtra(PASSWORD_VARIABLE_NAME);

        String nombreUsuario = nombreUsuarioExtra != null && !nombreUsuarioExtra.isEmpty() ? nombreUsuarioExtra : LOCAL_TESTS_USERNAME;
        String password = passwordExtra != null && !passwordExtra.isEmpty() ? passwordExtra : LOCAL_TESTS_PASSWORD;

        printVariableUsed(nombreUsuario, USERNAME_VARIABLE_NAME);
        printVariableUsed(password, PASSWORD_VARIABLE_NAME);

        onView(withId(R.id.button_log_in)).check(matches(isDisplayed()));

        onView(withId(R.id.button_log_in)).check(matches(withText(R.string.button_log_in)));

        onView(withId(R.id.edit_text_nombre)).perform(click());

        onView(withId(R.id.edit_text_nombre)).perform(typeText(nombreUsuario), closeSoftKeyboard());

        Thread.sleep(5000);

        onView(withId(R.id.edit_text_contrasenia)).perform(click());

        onView(withId(R.id.edit_text_contrasenia)).perform(typeText(password), closeSoftKeyboard());

        Thread.sleep(5000);

        onView(withId(R.id.button_log_in)).perform(click());

        Thread.sleep(5000);

        onView(withId(R.id.textView)).perform(scrollTo(), click());

        Thread.sleep(5000);

        onView(withId(R.id.textView)).perform(swipeDown());

        Thread.sleep(10000);

        Screengrab.screenshot("6-INICIO");

        onView(withId(R.id.navigation_documentos)).perform(click());

        Thread.sleep(5000);

        onView(withId(R.id.boton_asignaturas_marcadas)).perform(scrollTo(), click());

        Thread.sleep(10000);

        Screengrab.screenshot("5-ASIGNATURASMARCADAS");

        onView(withId(R.id.navigation_documentos)).perform(click());

        Thread.sleep(5000);

        onView(withId(R.id.boton_archivos_propios)).perform(scrollTo(), click());

        Thread.sleep(10000);

        Screengrab.screenshot("7-ARCHIVOSPROPIOS");

        onView(withId(R.id.navigation_buscar)).perform(click());

        onView(withId(R.id.spinner_facultad)).perform(click());

        onData(allOf(is(instanceOf(String.class)), is("Facultad de Ciencias de la Información"))).perform(click());

        onView(withId(R.id.spinner_grado)).perform(click());

        onData(allOf(is(instanceOf(String.class)), is("Grado en Comunicación Audiovisual"))).perform(click());

        onView(withId(R.id.spinner_curso)).perform(click());

        onData(allOf(is(instanceOf(String.class)), is("Primero"))).perform(click());

        onView(withId(R.id.spinner_asignatura)).perform(click());

        onData(allOf(is(instanceOf(String.class)), is("Historia del Cine"))).perform(click());

        Thread.sleep(10000);

        Screengrab.screenshot("3-BUSCADOR");

        onView(withId(R.id.boton_buscar)).perform(click());

        onView(withId(R.id.button_open_teoria)).perform(click());

        Thread.sleep(10000);

        Screengrab.screenshot("2-ARCHIVOSCATEGORIA");
    }

}
