package uah.gitlab.tablonuah.model;

public class Archivo {
    private int id;
    private String nombreArchivo;
    private String categoria;
    private int asignatura;
    private String autor;
    private int tamano;
    private String fecha;
    private String extension;

    public Archivo() {
    }

    public Archivo(int id, String nombreArchivo, String categoria, int asignatura, String autor, int tamano, String fecha, String extension) {
        this.id = id;
        this.nombreArchivo = nombreArchivo;
        this.categoria = categoria;
        this.asignatura = asignatura;
        this.autor = autor;
        this.tamano = tamano;
        this.fecha = fecha;
        this.extension = extension;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(int asignatura) {
        this.asignatura = asignatura;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getTamano() {
        return tamano;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public String toString() {
        return nombreArchivo;
    }
}
