package uah.gitlab.tablonuah.model;

public class Asignatura {
    private int id;
    private String nombre;
    private int idCurso;
    private String code;

    public Asignatura() {
    }

    public Asignatura(int id, String nombre, int idCurso, String code) {
        this.id = id;
        this.nombre = nombre;
        this.idCurso = idCurso;
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return id + " - " + nombre + " - " + idCurso + " - " + code;
    }
}
