
package uah.gitlab.tablonuah.model;

public class AsignaturaMarcada {

    private int idAsignatura;
    private String idUsuario;
    public AsignaturaMarcada() {
    }

    public AsignaturaMarcada(int idAsignatura, String idUsuario) {
        this.idAsignatura = idAsignatura;
        this.idUsuario = idUsuario;
    }

    public int getIdAsignatura() {
        return idAsignatura;
    }

    public void setIdAsignatura(int idAsignatura) {
        this.idAsignatura = idAsignatura;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public String toString() {
        return idAsignatura + " - " + idUsuario;
    }
}
