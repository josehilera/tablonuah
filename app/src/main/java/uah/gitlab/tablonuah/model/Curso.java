package uah.gitlab.tablonuah.model;

public class Curso {
    private int id;
    private String nombre;
    private int idGrado;

    public Curso() {
    }

    public Curso(int id, String nombre, int idFacultad) {
        this.id = id;
        this.nombre = nombre;
        this.idGrado = idGrado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(int idGrado) {
        this.idGrado = idGrado;
    }

    @Override
    public String toString() {
        return id + " - " + nombre + " - " + idGrado;
    }
}
