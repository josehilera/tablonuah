package uah.gitlab.tablonuah.model;

public class NovedadArchivo {

    private String fecha;
    private String usuarioEmisor;
    private int idArchivo;
    private int asignatura;

    public NovedadArchivo() {
        // empty constructor needed
    }

    public NovedadArchivo(String fecha, String usuarioEmisor, int idArchivo, int asignatura) {
        this.fecha = fecha;
        this.usuarioEmisor = usuarioEmisor;
        this.idArchivo = idArchivo;
        this.asignatura = asignatura;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUsuarioEmisor() {
        return usuarioEmisor;
    }

    public void setUsuarioEmisor(String usuarioEmisor) {
        this.usuarioEmisor = usuarioEmisor;
    }

    public int getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(int idArchivo) {
        this.idArchivo = idArchivo;
    }

    public int getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(int asignatura) {
        this.asignatura = asignatura;
    }

    @Override
    public String toString() {
        return usuarioEmisor + " - " + fecha;
    }
}
