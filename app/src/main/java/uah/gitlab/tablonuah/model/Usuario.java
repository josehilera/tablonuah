package uah.gitlab.tablonuah.model;

public class Usuario {
    private String nombreUsuario;
    private String password;
    private String extensionImagen;

    public Usuario() {
    }

    public Usuario(String nombreUsuario, String password) {
        this.nombreUsuario = nombreUsuario;
        this.password = password;
        this.extensionImagen = "none";
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getExtensionImagen() {
        return extensionImagen;
    }

    public void setExtensionImagen(String extensionImagen) {
        this.extensionImagen = extensionImagen;
    }

    @Override
    public String toString() {
        return nombreUsuario;
    }
}
