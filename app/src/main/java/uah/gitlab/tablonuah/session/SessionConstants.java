package uah.gitlab.tablonuah.session;

/**
 * Clase que constiene algunas constantes utilizadas en la aplicación.
 */
public class SessionConstants {
    public static final String USERNAME = "USERNAME";
    public static final String UPLOAD = "UPLOAD";

    public static final String EXTRA_ID = "ID";
    public static final String EXTRA_MENU_POSITION = "MENU_POSITION";
    public static final String EXTRA_CATEGORIA = "CATEGORIA";
    public static final String EXTRA_ID_USUARIO = "ID_USUARIO";
    public static final String EXTRA_TIPO_IMAGEN = "TIPO_IMAGEN";

    public static final int  PERMISSION_STORAGE_CODE = 1000;

}
