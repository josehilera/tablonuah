package uah.gitlab.tablonuah.ui;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.session.SessionConstants;
import uah.gitlab.tablonuah.utils.Links;

public class AdapterArchivosBusqueda extends FirestoreRecyclerAdapter<Archivo, AdapterArchivosBusqueda.ArchivoHolder> {

    Context context;

    int menuPosition = -1;
    OnFileTitleClickListener listener;

    public AdapterArchivosBusqueda(@NonNull FirestoreRecyclerOptions<Archivo> options, Context context, int menuPosition) {
        super(options);
        this.context = context;
        this.menuPosition = menuPosition;
    }

    @Override
    protected void onBindViewHolder(@NonNull ArchivoHolder holder, int position, @NonNull Archivo model) {
        SpannableString archivoSpan = Links.crearSpan(model.getNombreArchivo().length() > 30 ? model.getNombreArchivo().substring(0, 30) + "..." : model.getNombreArchivo(), v -> {
            Intent intent = new Intent(context, DisplayArchivo.class);
            intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
            intent.putExtra(SessionConstants.EXTRA_ID, model.getId());
            context.startActivity(intent);
        }, false, 0xFF1F13D3);
        holder.textViewName.setText(archivoSpan);
        Links.makeLinksFocusable(holder.textViewName);
    }

    @NonNull
    @Override
    public ArchivoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.archivo_busqueda_item, parent, false);
        return new ArchivoHolder(v);
    }

    public void deleteItem(int position) {
        getSnapshots().getSnapshot(position).getReference().delete();
    }

    class ArchivoHolder extends RecyclerView.ViewHolder {
        public final TextView textViewName;

        public ArchivoHolder(@NonNull View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.nombre_archivo);

            textViewName.setOnClickListener(view -> {
                int position = getBindingAdapterPosition();
                if (position != RecyclerView.NO_POSITION && listener != null) {
                    listener.onFileTitleClick(getSnapshots().getSnapshot(position), position);
                }
            });
        }
    }

    public interface OnFileTitleClickListener {
        // puedo añadir más cosas para enviar del adapter a la actividad, metiendo más parámetros en el método
        void onFileTitleClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnFileTitleClickListener(OnFileTitleClickListener listener) {
        this.listener = listener;
    }
}
