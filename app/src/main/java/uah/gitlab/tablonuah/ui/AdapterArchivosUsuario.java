package uah.gitlab.tablonuah.ui;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Asignatura;
import uah.gitlab.tablonuah.model.Curso;
import uah.gitlab.tablonuah.session.SessionConstants;
import uah.gitlab.tablonuah.utils.Links;

public class AdapterArchivosUsuario extends FirestoreRecyclerAdapter<Archivo, AdapterArchivosUsuario.ArchivoHolder> {

    Context context;
    OnFileTitleClickListener listener;

    int menuPosition = -1;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference asignaturasRef = db.collection("Asignatura");
    private CollectionReference cursosRef = db.collection("Curso");

    public AdapterArchivosUsuario(@NonNull FirestoreRecyclerOptions<Archivo> options, Context context, int menuPosition) {
        super(options);
        this.context = context;
        this.menuPosition = menuPosition;
    }

    @Override
    protected void onBindViewHolder(@NonNull ArchivoHolder holder, int position, @NonNull Archivo model) {
        SpannableString archivoSpan = Links.crearSpan(model.getNombreArchivo().length() > 20 ? model.getNombreArchivo().substring(0, 20) + "..." : model.getNombreArchivo(), v -> {
            Intent intent = new Intent(context, DisplayArchivo.class);
            intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
            intent.putExtra(SessionConstants.EXTRA_ID, model.getId());
            context.startActivity(intent);
        }, false, 0xFF1F13D3);
        holder.textViewName.setText(archivoSpan);
        Links.makeLinksFocusable(holder.textViewName);

        asignaturasRef.whereEqualTo("id", model.getAsignatura()).get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (!queryDocumentSnapshots.getDocuments().isEmpty()) {
                Asignatura asignatura = queryDocumentSnapshots.getDocuments().get(0).toObject(Asignatura.class);

                SpannableString asignaturaSpan = Links.crearSpan(asignatura.getCode(), v -> {
                    Intent intent = new Intent(context, ArchivosAsignatura.class);
                    intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
                    intent.putExtra(SessionConstants.EXTRA_ID, asignatura.getId());
                    context.startActivity(intent);
                }, false, 0xFF1F13D3);
                holder.textViewAsignatura.setText(asignaturaSpan);
                Links.makeLinksFocusable(holder.textViewAsignatura);

                cursosRef.whereEqualTo("id", asignatura.getIdCurso()).get().addOnSuccessListener(cursosQueryDocumentSnapshots -> {
                    if (!cursosQueryDocumentSnapshots.getDocuments().isEmpty()) {
                        Curso curso = cursosQueryDocumentSnapshots.getDocuments().get(0).toObject(Curso.class);

                        String cursoTexto = "";
                        switch (curso.getNombre()){
                            case "Primero":
                            case "Primer curso":
                            case "1":
                                cursoTexto = "1º";
                                break;
                            case "Segundo":
                            case "Segundo curso":
                            case "2":
                                cursoTexto = "2º";
                                break;
                            case "Tercero":
                            case "Tercer curso":
                            case "3":
                                cursoTexto = "3º";
                                break;
                            case "Cuarto":
                            case "Cuarto curso":
                            case "4":
                                cursoTexto = "4º";
                                break;
                            case "Quinto":
                            case "Quinto curso":
                            case "5":
                                cursoTexto = "5º";
                                break;
                            case "Sexto":
                            case "Sexto curso":
                            case "6":
                                cursoTexto = "6º";
                                break;
                        }

                        holder.textViewCurso.setText(cursoTexto);
                    }


                });
            }

        });
    }

    @NonNull
    @Override
    public ArchivoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.archivo_usuario_item, parent, false);
        return new ArchivoHolder(v);
    }

    public void deleteItem(int position) {
        getSnapshots().getSnapshot(position).getReference().delete();
    }

    class ArchivoHolder extends RecyclerView.ViewHolder {
        public final TextView textViewName;
        public final TextView textViewAsignatura;
        public final TextView textViewCurso;

        public ArchivoHolder(@NonNull View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.nombre_archivo_usuario);
            textViewName.setTextColor(0xFF1F13D3);
            textViewAsignatura = itemView.findViewById(R.id.nombre_asignatura);
            textViewCurso = itemView.findViewById(R.id.nombre_curso);

            /* implementado con un span en el onBindViewHolder
            textViewName.setOnClickListener(view -> {
                int position = getBindingAdapterPosition();
                if (position != RecyclerView.NO_POSITION && listener != null) {
                    listener.onFileTitleClick(getSnapshots().getSnapshot(position), position);
                }
            });*/
        }
    }

    public interface OnFileTitleClickListener {
        // puedo añadir más cosas para enviar del adapter a la actividad, metiendo más parámetros en el método
        void onFileTitleClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnFileTitleClickListener(OnFileTitleClickListener listener) {
        this.listener = listener;
    }
}
