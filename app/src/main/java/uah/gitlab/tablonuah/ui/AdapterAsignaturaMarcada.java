package uah.gitlab.tablonuah.ui;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Asignatura;
import uah.gitlab.tablonuah.model.AsignaturaMarcada;
import uah.gitlab.tablonuah.model.Curso;
import uah.gitlab.tablonuah.session.SessionConstants;
import uah.gitlab.tablonuah.utils.Links;

public class AdapterAsignaturaMarcada extends FirestoreRecyclerAdapter<AsignaturaMarcada, AdapterAsignaturaMarcada.AsignaturaMarcadaHolder> {

    Context context;
    OnStarClickListener listener;

    int menuPosition = -1;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference asignaturasRef = db.collection("Asignatura");
    private CollectionReference cursosRef = db.collection("Curso");

    public AdapterAsignaturaMarcada(@NonNull FirestoreRecyclerOptions<AsignaturaMarcada> options, Context context, int menuPosition) {
        super(options);
        this.context = context;
        this.menuPosition = menuPosition;
    }

    @Override
    protected void onBindViewHolder(@NonNull AsignaturaMarcadaHolder holder, int position, @NonNull AsignaturaMarcada model) {
        asignaturasRef.whereEqualTo("id", model.getIdAsignatura()).get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (!queryDocumentSnapshots.getDocuments().isEmpty()) {
                Asignatura asignatura = queryDocumentSnapshots.getDocuments().get(0).toObject(Asignatura.class);
                holder.textViewName.setText(asignatura.getNombre());

                SpannableString asignaturaSpan = Links.crearSpan(asignatura.getNombre(), v -> {
                    Intent intent = new Intent(context, ArchivosAsignatura.class);
                    intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
                    intent.putExtra(SessionConstants.EXTRA_ID, asignatura.getId());
                    context.startActivity(intent);
                }, false, 0xFF1F13D3);
                holder.textViewName.setText(asignaturaSpan);
                Links.makeLinksFocusable(holder.textViewName);

                cursosRef.whereEqualTo("id", asignatura.getIdCurso()).get().addOnSuccessListener(cursosQueryDocumentSnapshots -> {
                    if (!cursosQueryDocumentSnapshots.getDocuments().isEmpty()) {
                        Curso curso = cursosQueryDocumentSnapshots.getDocuments().get(0).toObject(Curso.class);

                        String cursoTexto = curso.getNombre();
                        switch (curso.getNombre()){
                            case "Primero":
                            case "Primer curso":
                            case "1":
                                cursoTexto = "1º";
                                break;
                            case "Segundo":
                            case "Segundo curso":
                            case "2":
                                cursoTexto = "2º";
                                break;
                            case "Tercero":
                            case "Tercer curso":
                            case "3":
                                cursoTexto = "3º";
                                break;
                            case "Cuarto":
                            case "Cuarto curso":
                            case "4":
                                cursoTexto = "4º";
                                break;
                            case "Quinto":
                            case "Quinto curso":
                            case "5":
                                cursoTexto = "5º";
                                break;
                            case "Sexto":
                            case "Sexto curso":
                            case "6":
                                cursoTexto = "6º";
                                break;
                        }

                        holder.textViewCurso.setText(cursoTexto);
                    }


                });
            }

        });
    }

    @NonNull
    @Override
    public AsignaturaMarcadaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.asignatura_marcada_item, parent, false);
        return new AsignaturaMarcadaHolder(v);
    }

    public void deleteItem(int position) {
        getSnapshots().getSnapshot(position).getReference().delete();
    }

    class AsignaturaMarcadaHolder extends RecyclerView.ViewHolder {
        public final TextView textViewName;
        public final TextView textViewCurso;
        public final ImageButton botonAsignatura;

        public AsignaturaMarcadaHolder(@NonNull View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.nombre_asignatura_marcada);
            textViewCurso = itemView.findViewById(R.id.curso_asignatura_marcada);
            botonAsignatura = itemView.findViewById(R.id.button_asignatura);

            botonAsignatura.setOnClickListener(view -> {
                int position = getBindingAdapterPosition();
                if (position != RecyclerView.NO_POSITION && listener != null) {
                    listener.onStarClick(getSnapshots().getSnapshot(position), position);
                }
            });
        }
    }

    public interface OnStarClickListener {
        // puedo añadir más cosas para enviar del adapter a la actividad, metiendo más parámetros en el método
        void onStarClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnStarClickListener(OnStarClickListener listener) {
        this.listener = listener;
    }
}
