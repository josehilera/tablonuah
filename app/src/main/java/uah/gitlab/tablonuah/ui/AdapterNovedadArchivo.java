package uah.gitlab.tablonuah.ui;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Asignatura;
import uah.gitlab.tablonuah.model.NovedadArchivo;
import uah.gitlab.tablonuah.session.SessionConstants;
import uah.gitlab.tablonuah.utils.Links;

public class AdapterNovedadArchivo extends FirestoreRecyclerAdapter<NovedadArchivo, AdapterNovedadArchivo.NovedadArchivoHolder> {

    Context context;
    OnTitleClickListener listener;

    int menuPosition = -1;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference archivosRef = db.collection("Archivo");
    private CollectionReference asignaturasRef = db.collection("Asignatura");

    public AdapterNovedadArchivo(@NonNull FirestoreRecyclerOptions<NovedadArchivo> options, Context context, int menuPosition) {
        super(options);
        this.context = context;
        this.menuPosition = menuPosition;
    }

    @Override
    protected void onBindViewHolder(@NonNull NovedadArchivoHolder holder, int position, @NonNull NovedadArchivo model) {
        archivosRef.whereEqualTo("id", model.getIdArchivo()).get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (!queryDocumentSnapshots.getDocuments().isEmpty()) {
                String nombreArchivo = queryDocumentSnapshots.getDocuments().get(0).toObject(Archivo.class).getNombreArchivo();

                asignaturasRef.whereEqualTo("id", model.getAsignatura()).get().addOnSuccessListener(asignaturaQueryDocumentSnapshots -> {
                            if (!asignaturaQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                String nombreAsignatura = asignaturaQueryDocumentSnapshots.getDocuments().get(0).toObject(Asignatura.class).getNombre();

                                String tiempo = obtenerDiferenciaTiempo(model.getFecha());

                                SpannableString usuarioEmisor = Links.crearSpan(model.getUsuarioEmisor(), v -> {
                                    Intent intent = new Intent(context, DisplayProfile.class);
                                    intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
                                    intent.putExtra(SessionConstants.EXTRA_ID, model.getUsuarioEmisor());
                                    context.startActivity(intent);
                                }, false, 0xFF1F13D3);

                                holder.textViewNovedadTexto.setText("El usuario ");

                                holder.textViewNovedadTexto.append(usuarioEmisor);

                                holder.textViewNovedadTexto.append(" ha subido ");

                                SpannableString nombreArchivoSpannable = Links.crearSpan(nombreArchivo.length() > 20 ? nombreArchivo.substring(0, 20) + "..." : nombreArchivo, view -> {
                                    Intent intent = new Intent(context, DisplayArchivo.class);
                                    intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
                                    intent.putExtra(SessionConstants.EXTRA_ID, model.getIdArchivo());
                                    context.startActivity(intent);
                                }, false, 0xFF1F13D3);

                                holder.textViewNovedadTexto.append(nombreArchivoSpannable);

                                SpannableString nombreAsignaturaSpannable = Links.crearSpan(nombreAsignatura, view -> {
                                    Intent intent = new Intent(context, ArchivosAsignatura.class);
                                    intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
                                    intent.putExtra(SessionConstants.EXTRA_ID, model.getAsignatura());
                                    context.startActivity(intent);
                                }, false, 0xFF1F13D3);

                                holder.textViewNovedadTexto.append(" a ");

                                holder.textViewNovedadTexto.append(nombreAsignaturaSpannable);

                                holder.textViewNovedadTexto.append(" hace " + tiempo);

                                Links.makeLinksFocusable(holder.textViewNovedadTexto);
                            }
                        });
            }
        });
    }

    private String obtenerDiferenciaTiempo(String fechaNovedad) {
        java.util.Date date = new java.util.Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechaActual = formatter.format(date);

        int numero = 0;
        String unidad = "";

        int anioNovedad = Integer.parseInt(fechaNovedad.substring(0, 4));
        int anioActual = Integer.parseInt(fechaActual.substring(0, 4));
        int mesNovedad = Integer.parseInt(fechaNovedad.substring(5, 7));
        int mesActual = Integer.parseInt(fechaActual.substring(5, 7));
        int diaNovedad = Integer.parseInt(fechaNovedad.substring(8, 10));
        int diaActual = Integer.parseInt(fechaActual.substring(8, 10));
        int horaNovedad = Integer.parseInt(fechaNovedad.substring(11, 13));
        int horaActual = Integer.parseInt(fechaActual.substring(11, 13));
        int minutoNovedad = Integer.parseInt(fechaNovedad.substring(14, 16));
        int minutoActual = Integer.parseInt(fechaActual.substring(14, 16));
        int segundoNovedad = Integer.parseInt(fechaNovedad.substring(17, 19));
        int segundoActual = Integer.parseInt(fechaActual.substring(17, 19));

        if (anioActual == anioNovedad) //Mismo año
            if (mesActual == mesNovedad) //Mismo mes
                if (diaActual == diaNovedad) //Mismo día
                    if (horaActual == horaNovedad) //Misma hora
                        if (minutoActual == minutoNovedad) { //Mismo minuto
                            numero = segundoActual - segundoNovedad;
                            unidad = numero > 1 ? " segundos" : " segundo";
                        }
                        else {
                            numero = minutoActual - minutoNovedad;
                            unidad = numero > 1 ? " minutos" : " minuto";
                        }
                    else {
                        numero = horaActual - horaNovedad;
                        unidad = numero > 1 ? " horas" : " hora";
                    }
                else {
                    numero = diaActual - diaNovedad;
                    unidad = numero > 1 ? " días" : " día";
                }
            else {
                numero = mesActual - mesNovedad;
                unidad = numero > 1 ? " meses" : " mes";
            }
        else {
            numero = anioActual - anioNovedad;
            unidad = numero > 1 ? " años" : " año";
        }
        String texto = numero + unidad;
        return texto;
    }

    @NonNull
    @Override
    public NovedadArchivoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.novedad_item, parent, false);
        return new NovedadArchivoHolder(v);
    }

    public void deleteItem(int position) {
        getSnapshots().getSnapshot(position).getReference().delete();
    }

    class NovedadArchivoHolder extends RecyclerView.ViewHolder {

        TextView textViewNovedadTexto;

        public NovedadArchivoHolder(@NonNull View itemView) {
            super(itemView);

            textViewNovedadTexto = itemView.findViewById(R.id.novedad_texto);

            textViewNovedadTexto.setOnClickListener(view -> {
                int position = getBindingAdapterPosition();
                if (position != RecyclerView.NO_POSITION && listener != null) {
                    listener.onTitleClick(getSnapshots().getSnapshot(position), position);
                }
            });
        }
    }

    public interface OnTitleClickListener {
        // puedo añadir más cosas para enviar del adapter a la actividad, metiendo más parámetros en el método
        void onTitleClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnTitleClickListener(OnTitleClickListener listener) {
        this.listener = listener;
    }
}
