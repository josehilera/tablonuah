package uah.gitlab.tablonuah.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.session.SessionConstants;

public class Apuntes extends AppCompatActivity {

    private static final int MENU_POSITION = 3;

    Button asignaturasMarcadas;
    Button archivos;

    BottomNavigationView bottomNavigationView;

    SharedPreferences settings;


    /**
     * Método onCreate en el que se asocian los elementos de la UI y sus correspondientes acciones al interactuar con ellos.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apuntes);

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        asignaturasMarcadas = findViewById(R.id.boton_asignaturas_marcadas);
        archivos = findViewById(R.id.boton_archivos_propios);
        bottomNavigationView = findViewById(R.id.bottom_navigation);

        asignaturasMarcadas.setOnClickListener(v -> {
            Intent intent = new Intent(Apuntes.this, AsignaturasMarcadas.class);
            startActivity(intent);
        });

        archivos.setOnClickListener(v -> {
            Intent intent = new Intent(Apuntes.this, ArchivosPropios.class);
            startActivity(intent);
        });

        bottomNavigationView.getMenu().getItem(MENU_POSITION).setChecked(true);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(Apuntes.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(Apuntes.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    return true;
                case R.id.navigation_subir:
                    Intent intentUploadFile= new Intent(Apuntes.this, UploadFile.class);
                    intentUploadFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUploadFile);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(Apuntes.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {

    }
}