package uah.gitlab.tablonuah.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Asignatura;
import uah.gitlab.tablonuah.model.AsignaturaMarcada;
import uah.gitlab.tablonuah.model.NovedadArchivo;
import uah.gitlab.tablonuah.session.SessionConstants;

public class ArchivosAsignatura extends AppCompatActivity {

    int idAsignatura = -1;

    int menuPosition = -1;

    Button button_marcar_asignatura;

    Button boton_subir_archivo;

    Button button_open_teoria;
    Button button_open_ejercicios;
    Button button_open_practicas;
    Button button_open_examenes;

    TextView titulo_asignatura;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference asignaturasRef = db.collection("Asignatura");
    private CollectionReference asignaturasMarcadasRef = db.collection("AsignaturaMarcada");

    SharedPreferences settings;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archivos_asignatura);

        Intent intent = getIntent();
        idAsignatura = intent.getIntExtra(SessionConstants.EXTRA_ID, -1);
        menuPosition = intent.getIntExtra(SessionConstants.EXTRA_MENU_POSITION, -1);

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        titulo_asignatura = findViewById(R.id.titulo_asignatura);

        button_marcar_asignatura = findViewById(R.id.boton_marcar_asignatura);

        boton_subir_archivo = findViewById(R.id.boton_subir_archivo);

        button_marcar_asignatura.setOnClickListener(v -> checkAsignaturaMarcada(true));

        boton_subir_archivo.setOnClickListener(v -> {
            Intent intentSubirArchivo = new Intent(this, UploadFile.class);
            intentSubirArchivo.putExtra(SessionConstants.EXTRA_ID, idAsignatura);
            intentSubirArchivo.putExtra(SessionConstants.EXTRA_MENU_POSITION, menuPosition);
            settings.edit().putBoolean(SessionConstants.UPLOAD, true).apply();
            startActivity(intentSubirArchivo);
        });

        button_open_teoria = findViewById(R.id.button_open_teoria);
        button_open_ejercicios = findViewById(R.id.button_open_ejercicios);
        button_open_practicas = findViewById(R.id.button_open_practicas);
        button_open_examenes = findViewById(R.id.button_open_examenes);

        button_open_teoria.setOnClickListener(getCategoriaOnClickListener("Teoría"));
        button_open_ejercicios.setOnClickListener(getCategoriaOnClickListener("Ejercicios"));
        button_open_practicas.setOnClickListener(getCategoriaOnClickListener("Prácticas"));
        button_open_examenes.setOnClickListener(getCategoriaOnClickListener("Exámenes"));

        asignaturasRef.whereEqualTo("id", idAsignatura).get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (!queryDocumentSnapshots.getDocuments().isEmpty()) {
                titulo_asignatura.setText(queryDocumentSnapshots.getDocuments().get(0).toObject(Asignatura.class).getNombre());
            }
        });

        bottomNavigationView.getMenu().getItem(menuPosition).setChecked(true);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(ArchivosAsignatura.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(ArchivosAsignatura.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    Intent intentApuntes = new Intent(ArchivosAsignatura.this, Apuntes.class);
                    intentApuntes.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentApuntes);
                    finish();
                    return true;
                case R.id.navigation_subir:
                    Intent intentUploadFile= new Intent(ArchivosAsignatura.this, UploadFile.class);
                    intentUploadFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUploadFile);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(ArchivosAsignatura.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });

        checkAsignaturaMarcada(false);
    }

    private View.OnClickListener getCategoriaOnClickListener(String categoria) {
        return view -> {
            Intent intentCategoria = new Intent(this, ArchivosCategoria.class);
            intentCategoria.putExtra(SessionConstants.EXTRA_CATEGORIA, categoria);
            intentCategoria.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
            intentCategoria.putExtra(SessionConstants.EXTRA_ID, idAsignatura);
            startActivity(intentCategoria);
        };
    }

    private void checkAsignaturaMarcada(boolean buttonClicked) {
        asignaturasMarcadasRef.whereEqualTo("idUsuario", settings.getString(SessionConstants.USERNAME, "InvalidUser")).get().addOnSuccessListener(queryDocumentSnapshots -> {
            boolean isAsignaturaMarcada = false;

            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                if (documentSnapshot.toObject(AsignaturaMarcada.class).getIdAsignatura() == idAsignatura) {
                    if (buttonClicked) {
                        // al hacer el click estaba marcada
                        //      --> desmarcamos
                        //          --> eliminar AsignaturaMarcada
                        //          --> cambiar texto a "MARCAR ASIGNATURA"
                        asignaturasMarcadasRef.whereEqualTo("idUsuario", settings.getString(SessionConstants.USERNAME, "InvalidUser")).whereEqualTo("idAsignatura", idAsignatura).get().addOnSuccessListener(asignaturaMarcadaQueryDocumentSnapshots -> {
                            if (!asignaturaMarcadaQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                asignaturaMarcadaQueryDocumentSnapshots.getDocuments().get(0).getReference().delete();
                            }
                        });

                        button_marcar_asignatura.setText(R.string.marcar_asignatura);
                        button_marcar_asignatura.setBackground(ContextCompat.getDrawable(this, R.drawable.layout_bg_green));
                        button_marcar_asignatura.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_marcar, 0, 0, 0);

                        Toast.makeText(this, "Asignatura desmarcada", Toast.LENGTH_SHORT).show();
                    } else {
                        // acabamos de cargar la pantalla
                        button_marcar_asignatura.setText(R.string.desmarcar_asignatura);
                        button_marcar_asignatura.setBackground(ContextCompat.getDrawable(this, R.drawable.layout_bg_red));
                        button_marcar_asignatura.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_desmarcar, 0, 0, 0);
                    }
                    isAsignaturaMarcada = true;
                }
            }

            if (!isAsignaturaMarcada) {
                if (buttonClicked) {
                    // al hacer el click estaba desmarcada
                    //      --> marcamos
                    //          --> postear AsignaturaMarcada
                    //          --> cambiar texto a "DESMARCAR ASIGNATURA"
                    AsignaturaMarcada asignaturaMarcada = new AsignaturaMarcada(idAsignatura, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    asignaturasMarcadasRef.document().set(asignaturaMarcada);

                    button_marcar_asignatura.setText(R.string.desmarcar_asignatura);
                    button_marcar_asignatura.setBackground(ContextCompat.getDrawable(this, R.drawable.layout_bg_red));
                    button_marcar_asignatura.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_desmarcar, 0, 0, 0);

                    Toast.makeText(this, "Asignatura marcada", Toast.LENGTH_SHORT).show();
                } else {
                    // acabamos de cargar la pantalla
                    button_marcar_asignatura.setText(R.string.marcar_asignatura);
                    button_marcar_asignatura.setBackground(ContextCompat.getDrawable(this, R.drawable.layout_bg_green));
                    button_marcar_asignatura.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_marcar, 0, 0, 0);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAsignaturaMarcada(false);
    }
}