package uah.gitlab.tablonuah.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Asignatura;
import uah.gitlab.tablonuah.model.AsignaturaMarcada;
import uah.gitlab.tablonuah.session.SessionConstants;

public class ArchivosCategoria extends AppCompatActivity {

    int idAsignatura = -1;

    int menuPosition = -1;

    Button button_marcar_asignatura;

    Button boton_subir_archivo;

    TextView titulo_asignatura;
    TextView titulo_categoria;

    String categoria;

    TextView sin_archivos_categoria;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference archivosRef = db.collection("Archivo");
    private CollectionReference asignaturasRef = db.collection("Asignatura");
    private CollectionReference asignaturasMarcadasRef = db.collection("AsignaturaMarcada");

    private AdapterArchivosBusqueda adapter;

    SharedPreferences settings;

    RecyclerView recyclerView;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archivos_categoria);

        Intent intent = getIntent();
        idAsignatura = intent.getIntExtra(SessionConstants.EXTRA_ID, -1);
        menuPosition = intent.getIntExtra(SessionConstants.EXTRA_MENU_POSITION, -1);
        categoria = intent.getStringExtra(SessionConstants.EXTRA_CATEGORIA);

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        setUpRecyclerView();

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        recyclerView.setVisibility(View.GONE);

        titulo_asignatura = findViewById(R.id.titulo_asignatura);

        titulo_categoria = findViewById(R.id.titulo_categoria);

        sin_archivos_categoria = findViewById(R.id.no_archivos_categoria);

        button_marcar_asignatura = findViewById(R.id.boton_marcar_asignatura);

        boton_subir_archivo = findViewById(R.id.boton_subir_archivo);

        button_marcar_asignatura.setOnClickListener(v -> checkAsignaturaMarcada(true));

        boton_subir_archivo.setOnClickListener(v -> {
            Intent intentSubirArchivo = new Intent(this, UploadFile.class);
            intentSubirArchivo.putExtra(SessionConstants.EXTRA_CATEGORIA, categoria);
            intentSubirArchivo.putExtra(SessionConstants.EXTRA_MENU_POSITION, menuPosition);
            intentSubirArchivo.putExtra(SessionConstants.EXTRA_ID, idAsignatura);
            settings.edit().putBoolean(SessionConstants.UPLOAD, true).apply();
            startActivity(intentSubirArchivo);
        });

        asignaturasRef.whereEqualTo("id", idAsignatura).get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (!queryDocumentSnapshots.getDocuments().isEmpty()) {
                titulo_asignatura.setText(queryDocumentSnapshots.getDocuments().get(0).toObject(Asignatura.class).getNombre());
            }
        });

        titulo_categoria.setText(categoria);

        bottomNavigationView.getMenu().getItem(menuPosition).setChecked(true);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(ArchivosCategoria.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(ArchivosCategoria.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    Intent intentApuntes = new Intent(ArchivosCategoria.this, Apuntes.class);
                    intentApuntes.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentApuntes);
                    finish();
                    return true;
                case R.id.navigation_subir:
                    Intent intentUploadFile= new Intent(ArchivosCategoria.this, UploadFile.class);
                    intentUploadFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUploadFile);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(ArchivosCategoria.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });

        checkArchivosCategoriaExistance();
        checkAsignaturaMarcada(false);
    }

    private void setUpRecyclerView() {
        Query query = archivosRef.whereEqualTo("categoria", categoria).whereEqualTo("asignatura", idAsignatura);

        FirestoreRecyclerOptions<Archivo> options = new FirestoreRecyclerOptions.Builder<Archivo>()
                .setQuery(query, Archivo.class)
                .build();

        adapter = new AdapterArchivosBusqueda(options, this, menuPosition);

        recyclerView = findViewById(R.id.archivos_categoria_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(null);

        adapter.setOnFileTitleClickListener(getOnFileTitleClickListener());
    }

    private AdapterArchivosBusqueda.OnFileTitleClickListener getOnFileTitleClickListener() {
        return (documentSnapshot, position) -> {
            Archivo archivo = documentSnapshot.toObject(Archivo.class);
            Intent intent = new Intent(this, DisplayArchivo.class);
            intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
            intent.putExtra(SessionConstants.EXTRA_ID, archivo.getId());
            startActivity(intent);
        };
    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        adapter.stopListening();
    }

    private void checkAsignaturaMarcada(boolean buttonClicked) {
        asignaturasMarcadasRef.whereEqualTo("idUsuario", settings.getString(SessionConstants.USERNAME, "InvalidUser")).get().addOnSuccessListener(queryDocumentSnapshots -> {
            boolean isAsignaturaMarcada = false;

            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                if (documentSnapshot.toObject(AsignaturaMarcada.class).getIdAsignatura() == idAsignatura) {
                    if (buttonClicked) {
                        // al hacer el click estaba marcada
                        //      --> desmarcamos
                        //          --> eliminar AsignaturaMarcada
                        //          --> cambiar texto a "MARCAR ASIGNATURA"
                        asignaturasMarcadasRef.whereEqualTo("idUsuario", settings.getString(SessionConstants.USERNAME, "InvalidUser")).whereEqualTo("idAsignatura", idAsignatura).get().addOnSuccessListener(asignaturaMarcadaQueryDocumentSnapshots -> {
                            if (!asignaturaMarcadaQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                asignaturaMarcadaQueryDocumentSnapshots.getDocuments().get(0).getReference().delete();
                            }
                        });

                        button_marcar_asignatura.setText(R.string.marcar_asignatura);
                        button_marcar_asignatura.setBackground(ContextCompat.getDrawable(this, R.drawable.layout_bg_green));
                        button_marcar_asignatura.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_marcar, 0, 0, 0);

                        Toast.makeText(this, "Asignatura desmarcada", Toast.LENGTH_SHORT).show();
                    } else {
                        // acabamos de cargar la pantalla
                        button_marcar_asignatura.setText(R.string.desmarcar_asignatura);
                        button_marcar_asignatura.setBackground(ContextCompat.getDrawable(this, R.drawable.layout_bg_red));
                        button_marcar_asignatura.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_desmarcar, 0, 0, 0);
                    }
                    isAsignaturaMarcada = true;
                }
            }

            if (!isAsignaturaMarcada) {
                if (buttonClicked) {
                    // al hacer el click estaba desmarcada
                    //      --> marcamos
                    //          --> postear AsignaturaMarcada
                    //          --> cambiar texto a "DESMARCAR ASIGNATURA"
                    AsignaturaMarcada asignaturaMarcada = new AsignaturaMarcada(idAsignatura, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    asignaturasMarcadasRef.document().set(asignaturaMarcada);

                    button_marcar_asignatura.setText(R.string.desmarcar_asignatura);
                    button_marcar_asignatura.setBackground(ContextCompat.getDrawable(this, R.drawable.layout_bg_red));
                    button_marcar_asignatura.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_desmarcar, 0, 0, 0);

                    Toast.makeText(this, "Asignatura marcada", Toast.LENGTH_SHORT).show();
                } else {
                    // acabamos de cargar la pantalla
                    button_marcar_asignatura.setText(R.string.marcar_asignatura);
                    button_marcar_asignatura.setBackground(ContextCompat.getDrawable(this, R.drawable.layout_bg_green));
                    button_marcar_asignatura.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_marcar, 0, 0, 0);
                }
            }
        });
    }

    private void checkArchivosCategoriaExistance() {
        archivosRef.whereEqualTo("categoria", categoria).whereEqualTo("asignatura", idAsignatura).get().addOnSuccessListener(archivosTeoriaQueryDocumentSnapshots -> {
            if (archivosTeoriaQueryDocumentSnapshots.getDocuments().isEmpty()) {
                recyclerView.setVisibility(View.GONE);
                sin_archivos_categoria.setVisibility(View.VISIBLE);

                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) boton_subir_archivo.getLayoutParams();
                params.topMargin = 280;
                boton_subir_archivo.setLayoutParams(params);
            }
            else {
                recyclerView.setVisibility(View.VISIBLE);
                sin_archivos_categoria.setVisibility(View.GONE);

                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) boton_subir_archivo.getLayoutParams();
                params.topMargin = 100;
                boton_subir_archivo.setLayoutParams(params);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAsignaturaMarcada(false);
        checkArchivosCategoriaExistance();
    }
}