package uah.gitlab.tablonuah.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.session.SessionConstants;

public class ArchivosPropios extends AppCompatActivity {

    private static final int MENU_POSITION = 3;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference archivosRef = db.collection("Archivo");

    private AdapterArchivosUsuario adapter;

    SharedPreferences settings;

    TextView noArchivos;
    TextView tituloArchivo;
    TextView tituloAsignatura;
    TextView tituloCurso;

    RecyclerView recyclerView;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archivos_propios);

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        setUpRecyclerView();

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        noArchivos = findViewById(R.id.no_archivos);
        tituloArchivo = findViewById(R.id.titulo_archivo);
        tituloAsignatura = findViewById(R.id.titulo_asignatura);
        tituloCurso = findViewById(R.id.titulo_curso);

        recyclerView.setVisibility(View.GONE);
        tituloArchivo.setVisibility(View.GONE);
        tituloAsignatura.setVisibility(View.GONE);
        tituloCurso.setVisibility(View.GONE);

        bottomNavigationView.getMenu().getItem(MENU_POSITION).setChecked(true);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(ArchivosPropios.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(ArchivosPropios.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    super.onBackPressed();
                    finish();
                    return true;
                case R.id.navigation_subir:
                    Intent intentUploadFile= new Intent(ArchivosPropios.this, UploadFile.class);
                    intentUploadFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUploadFile);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(ArchivosPropios.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });

        checkArchivosPropiosExistance();
    }

    private void setUpRecyclerView() {
        Query query = archivosRef.whereEqualTo("autor", settings.getString(SessionConstants.USERNAME, "InvalidUser"));

        FirestoreRecyclerOptions<Archivo> options = new FirestoreRecyclerOptions.Builder<Archivo>()
                .setQuery(query, Archivo.class)
                .build();

        adapter = new AdapterArchivosUsuario(options, this, MENU_POSITION);

        recyclerView = findViewById(R.id.archivos_usuario_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(null);

        /* implementado con un span en el onBindViewHolder del adapter
        adapter.setOnFileTitleClickListener((documentSnapshot, position) -> {
            int idArchivo = archivosList.get(position).getId();

            Intent intent = new Intent(context, DisplayArchivo.class);
            intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, MENU_POSITION);
            intent.putExtra(SessionConstants.EXTRA_ID, idArchivo);
            startActivity(intent);
        });*/
    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        adapter.stopListening();
    }

    private void checkArchivosPropiosExistance() {
        archivosRef.whereEqualTo("autor", settings.getString(SessionConstants.USERNAME, "InvalidUser")).get().addOnSuccessListener(archivosPropiosQueryDocumentSnapshots -> {
            if (archivosPropiosQueryDocumentSnapshots.getDocuments().isEmpty()) {
                noArchivos.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                tituloArchivo.setVisibility(View.GONE);
                tituloAsignatura.setVisibility(View.GONE);
                tituloCurso.setVisibility(View.GONE);
            }
            else {
                TextView tituloMisArchivos = findViewById(R.id.titulo_mis_archivos);
                tituloMisArchivos.setText(getString(R.string.titulo_archivos) + " (" + archivosPropiosQueryDocumentSnapshots.getDocuments().size() + ")");

                noArchivos.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                tituloArchivo.setVisibility(View.VISIBLE);
                tituloAsignatura.setVisibility(View.VISIBLE);
                tituloCurso.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkArchivosPropiosExistance();
    }
}