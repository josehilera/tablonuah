package uah.gitlab.tablonuah.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.AsignaturaMarcada;
import uah.gitlab.tablonuah.session.SessionConstants;

public class AsignaturasMarcadas extends AppCompatActivity {

    private static final int MENU_POSITION = 3;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference asignaturasMarcadasRef = db.collection("AsignaturaMarcada");

    private AdapterAsignaturaMarcada adapter;

    SharedPreferences settings;

    TextView noAsignaturasMarcadas;
    TextView tituloCurso;
    TextView tituloAsignatura;

    RecyclerView recyclerView;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asignaturas_marcadas);

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        setUpRecyclerView();

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        noAsignaturasMarcadas = findViewById(R.id.no_asignaturas_marcadas);
        tituloCurso = findViewById(R.id.titulo_curso);
        tituloAsignatura = findViewById(R.id.titulo_asignatura);

        recyclerView.setVisibility(View.GONE);
        tituloCurso.setVisibility(View.GONE);
        tituloAsignatura.setVisibility(View.GONE);

        bottomNavigationView.getMenu().getItem(MENU_POSITION).setChecked(true);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(AsignaturasMarcadas.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(AsignaturasMarcadas.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    super.onBackPressed();
                    finish();
                    return true;
                case R.id.navigation_subir:
                    Intent intentUploadFile= new Intent(AsignaturasMarcadas.this, UploadFile.class);
                    intentUploadFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUploadFile);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(AsignaturasMarcadas.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });

        checkAsignaturasMarcadasExistance();
    }

    private void setUpRecyclerView() {
        Query query = asignaturasMarcadasRef.whereEqualTo("idUsuario", settings.getString(SessionConstants.USERNAME, "InvalidUser")).orderBy("idAsignatura", Query.Direction.ASCENDING);

        FirestoreRecyclerOptions<AsignaturaMarcada> options = new FirestoreRecyclerOptions.Builder<AsignaturaMarcada>()
                .setQuery(query, AsignaturaMarcada.class)
                .build();

        adapter = new AdapterAsignaturaMarcada(options, this, MENU_POSITION);

        recyclerView = findViewById(R.id.asignaturas_marcadas_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(null);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AsignaturasMarcadas.this);
                builder.setCancelable(true);
                builder.setTitle("Desmarcar asignatura");
                builder.setMessage("¿Seguro que quieres desmarcar la asignatura?");
                builder.setPositiveButton("DESMARCAR",
                        (dialog, which) -> {
                            adapter.deleteItem(viewHolder.getAbsoluteAdapterPosition());
                            Toast.makeText(AsignaturasMarcadas.this, "Asignatura desmarcada", Toast.LENGTH_SHORT).show();
                            checkAsignaturasMarcadasExistance();
                        });
                builder.setNegativeButton("CANCELAR", (dialog, which) -> {});

                builder.setOnDismissListener(dialogInterface -> recyclerView.getAdapter().notifyDataSetChanged());

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }).attachToRecyclerView(recyclerView);

        adapter.setOnStarClickListener((documentSnapshot, position) -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(AsignaturasMarcadas.this);
            builder.setCancelable(true);
            builder.setTitle("Desmarcar asignatura");
            builder.setMessage("¿Seguro que quieres desmarcar la asignatura?");
            builder.setPositiveButton("DESMARCAR",
                    (dialog, which) -> {
                        documentSnapshot.getReference().delete();
                        Toast.makeText(AsignaturasMarcadas.this, "Asignatura desmarcada", Toast.LENGTH_SHORT).show();
                        checkAsignaturasMarcadasExistance();
                    });
            builder.setNegativeButton("CANCELAR", (dialog, which) -> {
            });

            AlertDialog dialog = builder.create();
            dialog.show();

        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        adapter.stopListening();
    }

    private void checkAsignaturasMarcadasExistance() {
        asignaturasMarcadasRef.whereEqualTo("idUsuario", settings.getString(SessionConstants.USERNAME, "InvalidUser")).get().addOnSuccessListener(asignaturasMarcadasQueryDocumentSnapshots -> {
            if (asignaturasMarcadasQueryDocumentSnapshots.getDocuments().isEmpty()) {
                recyclerView.setVisibility(View.GONE);
                tituloCurso.setVisibility(View.GONE);
                tituloAsignatura.setVisibility(View.GONE);
                noAsignaturasMarcadas.setVisibility(View.VISIBLE);
            }
            else {
                recyclerView.setVisibility(View.VISIBLE);
                tituloCurso.setVisibility(View.VISIBLE);
                tituloAsignatura.setVisibility(View.VISIBLE);
                noAsignaturasMarcadas.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkAsignaturasMarcadasExistance();
    }
}