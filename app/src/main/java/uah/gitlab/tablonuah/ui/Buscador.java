package uah.gitlab.tablonuah.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.OpenableColumns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Asignatura;
import uah.gitlab.tablonuah.model.Curso;
import uah.gitlab.tablonuah.model.Facultad;
import uah.gitlab.tablonuah.model.Grado;
import uah.gitlab.tablonuah.model.NovedadArchivo;
import uah.gitlab.tablonuah.session.SessionConstants;

public class Buscador extends AppCompatActivity {

    private static final int MENU_POSITION = 1;
    private CollectionReference facultadCollectionReference;
    private CollectionReference gradoCollectionReference;
    private CollectionReference cursoCollectionReference;
    private CollectionReference asignaturaCollectionReference;

    Button botonBuscar;
    BottomNavigationView bottomNavigationView;

    SharedPreferences settings;

    private Spinner spinner_facultad;
    private Spinner spinner_grado;
    private Spinner spinner_curso;
    private Spinner spinner_asignatura;

    List<String> listaFacultadesString;
    List<String> listaGradosString;
    List<String> listaCursosString;
    List<String> listaAsignaturasString;

    String nombreFacultadSeleccionada;
    String nombreGradoSeleccionado;
    String nombreCursoSeleccionado;
    String nombreAsignaturaSeleccionada;

    List<Facultad> facultadesList = new ArrayList<>();
    List<Grado> gradosList = new ArrayList<>();
    List<Curso> cursosList = new ArrayList<>();
    List<Asignatura> asignaturasList = new ArrayList<>();

    int idAsignaturaMarcada = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buscador);

        facultadCollectionReference = FirebaseFirestore.getInstance().collection("Facultad");
        gradoCollectionReference = FirebaseFirestore.getInstance().collection("Grado");
        cursoCollectionReference = FirebaseFirestore.getInstance().collection("Curso");
        asignaturaCollectionReference = FirebaseFirestore.getInstance().collection("Asignatura");

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        spinner_facultad = findViewById(R.id.spinner_facultad);
        spinner_grado = findViewById(R.id.spinner_grado);
        spinner_curso = findViewById(R.id.spinner_curso);
        spinner_asignatura = findViewById(R.id.spinner_asignatura);

        botonBuscar = findViewById(R.id.boton_buscar);

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idAsignaturaMarcada != -1) {
                    Intent intent = new Intent(Buscador.this, ArchivosAsignatura.class);
                    intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, MENU_POSITION);
                    intent.putExtra(SessionConstants.EXTRA_ID, idAsignaturaMarcada);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(Buscador.this, "Selecciona una asignatura", Toast.LENGTH_LONG).show();
                }
            }
        });

        bottomNavigationView.getMenu().getItem(MENU_POSITION).setChecked(true);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(Buscador.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    loadUniversity();
                    return true;
                case R.id.navigation_documentos:
                    Intent intentApuntes = new Intent(Buscador.this, Apuntes.class);
                    intentApuntes.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentApuntes);
                    finish();
                    return true;
                case R.id.navigation_subir:
                    Intent intentUploadFile= new Intent(Buscador.this, UploadFile.class);
                    intentUploadFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUploadFile);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(Buscador.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });

        loadUniversity();
    }

    /**
     * Este método se encarga de llama a los otros métodos que cargan las facultades, grados, cursos y asignaturas para introducirlos en los spinners.
     */
    private void loadUniversity() {
        spinner_grado.setEnabled(false);
        spinner_curso.setEnabled(false);
        spinner_asignatura.setEnabled(false);

        loadFacultades();
        loadGrados();
        loadCursos();
        loadAsignaturas();
    }

    /**
     * Aquí se inicializa el spinner de las facultades y se cargan los grados correspondientes a la facultad seleccionada.
     */
    private void loadFacultades() {
        listaFacultadesString = new ArrayList<String>();

        listaFacultadesString.add("Elija una facultad");

        ArrayAdapter<String> adapterFacultad = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaFacultadesString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterFacultad.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_facultad.setAdapter(adapterFacultad);

        spinner_facultad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreFacultadSeleccionada = (String) parent.getSelectedItem();

                    spinner_grado.setEnabled(true);

                    idAsignaturaMarcada = -1;

                    listaCursosString.clear();
                    listaCursosString.add("Elija un curso");
                    spinner_curso.setSelection(0);
                    spinner_curso.setEnabled(false);

                    listaAsignaturasString.clear();
                    listaAsignaturasString.add("Elija una asignatura");
                    spinner_asignatura.setSelection(0);
                    spinner_asignatura.setEnabled(false);

                    gradoCollectionReference.whereEqualTo("idFacultad", facultadesList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                        gradosList.clear();
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            gradosList.add(documentSnapshot.toObject(Grado.class));
                        }
                        listaGradosString.clear();

                        listaGradosString.add("Elija un grado");

                        for(Grado grado : gradosList) {
                            listaGradosString.add(grado.getNombre());
                        }

                        spinner_grado.setSelection(0);
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        facultadCollectionReference.orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
            facultadesList.clear();
            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                facultadesList.add(documentSnapshot.toObject(Facultad.class));
            }
            listaFacultadesString.clear();

            listaFacultadesString.add("Elija una facultad");

            for(Facultad facultad : facultadesList) {
                listaFacultadesString.add(facultad.getNombre());
            }
        });
    }


    /**
     * Aquí se inicializa el spinner de los grados y se cargan los cursos correspondientes al grado seleccionado.
     */
    private void loadGrados() {
        listaGradosString = new ArrayList<String>();

        listaGradosString.add("Elija un grado");

        ArrayAdapter<String> adapterGrado = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaGradosString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterGrado.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_grado.setAdapter(adapterGrado);

        spinner_grado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreGradoSeleccionado = (String) parent.getSelectedItem();

                    spinner_curso.setEnabled(true);

                    idAsignaturaMarcada = -1;

                    listaAsignaturasString.clear();
                    listaAsignaturasString.add("Elija una asignatura");
                    spinner_asignatura.setSelection(0);
                    spinner_asignatura.setEnabled(false);

                    cursoCollectionReference.whereEqualTo("idGrado", gradosList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                        cursosList.clear();
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            cursosList.add(documentSnapshot.toObject(Curso.class));
                        }
                        listaCursosString.clear();

                        listaCursosString.add("Elija un curso");

                        for (Curso curso : cursosList) {
                            listaCursosString.add(curso.getNombre());
                        }

                        spinner_curso.setSelection(0);
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Aquí se inicializa el spinner de los cursos y se cargan las asignaturas correspondientes al curso seleccionado.
     */
    private void loadCursos() {
        listaCursosString = new ArrayList<String>();

        listaCursosString.add("Elija un curso");

        ArrayAdapter<String> adapterCurso = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaCursosString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterCurso.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_curso.setAdapter(adapterCurso);

        spinner_curso.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreCursoSeleccionado = (String) parent.getSelectedItem();

                    spinner_asignatura.setEnabled(true);

                    idAsignaturaMarcada = -1;

                    asignaturaCollectionReference.whereEqualTo("idCurso", cursosList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                        asignaturasList.clear();
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            asignaturasList.add(documentSnapshot.toObject(Asignatura.class));
                        }
                        listaAsignaturasString.clear();

                        listaAsignaturasString.add("Elija una asignatura");

                        for(Asignatura asignatura : asignaturasList) {
                            listaAsignaturasString.add(asignatura.getNombre());
                        }

                        spinner_asignatura.setSelection(0);
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    /**
     * Aquí se inicializa el spinner de las asignaturas y se guarda el id de la que el usuario seleccione.
     */
    private void loadAsignaturas() {
        listaAsignaturasString = new ArrayList<String>();

        listaAsignaturasString.add("Elija una asignatura");

        ArrayAdapter<String> adapterAsignatura = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaAsignaturasString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterAsignatura.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_asignatura.setAdapter(adapterAsignatura);

        spinner_asignatura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreAsignaturaSeleccionada = (String) parent.getSelectedItem();

                    for (Asignatura a : asignaturasList) {
                        if (a.getNombre().equals(nombreAsignaturaSeleccionada)) {
                            idAsignaturaMarcada = a.getId();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {

    }
}