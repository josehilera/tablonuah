package uah.gitlab.tablonuah.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Asignatura;
import uah.gitlab.tablonuah.session.SessionConstants;
import uah.gitlab.tablonuah.utils.Links;

public class DisplayArchivo extends AppCompatActivity {

    Context context;

    int menuPosition;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference archivosRef = db.collection("Archivo");
    private CollectionReference asignaturasRef = db.collection("Asignatura");
    private CollectionReference novedadesArchivosRef = db.collection("NovedadArchivo");
    private StorageReference storageReference;

    Archivo archivo;

    TextView nombre;
    TextView tamanioNoAutor;
    TextView tamanioAutor;
    TextView asignatura;
    TextView autor;
    TextView fecha;
    ImageView imagen;
    Button botonDescargarNoAutor;
    Button botonDescargarAutor;
    ImageButton botonEditarNombre;
    Button botonBorrar;
    SharedPreferences settings;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_archivo);

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        context = this;

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        storageReference = FirebaseStorage.getInstance().getReference("Archivo");

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        int idArchivo = intent.getIntExtra(SessionConstants.EXTRA_ID, -1);
        menuPosition = intent.getIntExtra(SessionConstants.EXTRA_MENU_POSITION, -1);

        nombre = findViewById(R.id.display_archivo_nombre);
        tamanioNoAutor = findViewById(R.id.display_archivo_tamanio_no_autor);
        tamanioAutor = findViewById(R.id.display_archivo_tamanio_autor);
        asignatura = findViewById(R.id.display_archivo_asignatura);
        autor = findViewById(R.id.display_archivo_autor);
        fecha = findViewById(R.id.display_archivo_fecha);
        imagen = findViewById(R.id.display_archivo_imagen);

        imagen.setOnClickListener(view -> {
            Intent intentFullScreenImage = new Intent(context, DisplayImageFullScreen.class);
            intentFullScreenImage.putExtra(SessionConstants.EXTRA_ID, idArchivo);
            intentFullScreenImage.putExtra(SessionConstants.EXTRA_TIPO_IMAGEN, "ARCHIVO");
            context.startActivity(intentFullScreenImage);
            /*
            // BEGIN_INCLUDE (get_current_ui_flags)
            // The UI options currently enabled are represented by a bitfield.
            // getSystemUiVisibility() gives us that bitfield.
            int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
            int newUiOptions = uiOptions;
            // END_INCLUDE (get_current_ui_flags)
            // BEGIN_INCLUDE (toggle_ui_flags)
            boolean isImmersiveModeEnabled =
                    ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
            if (isImmersiveModeEnabled) {
                Log.i(TAG, "Turning immersive mode mode off. ");
            } else {
                Log.i(TAG, "Turning immersive mode mode on.");
            }

            // Navigation bar hiding:  Backwards compatible to ICS.
            if (Build.VERSION.SDK_INT >= 14) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            }

            // Status bar hiding: Backwards compatible to Jellybean
            if (Build.VERSION.SDK_INT >= 16) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
            }

            // Immersive mode: Backward compatible to KitKat.
            // Note that this flag doesn't do anything by itself, it only augments the behavior
            // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
            // all three flags are being toggled together.
            // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
            // Sticky immersive mode differs in that it makes the navigation and status bars
            // semi-transparent, and the UI flag does not get cleared when the user interacts with
            // the screen.
            if (Build.VERSION.SDK_INT >= 18) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }

            getWindow().getDecorView().setSystemUiVisibility(newUiOptions);

            //END_INCLUDE (set_ui_flags)*/
        });

        botonDescargarNoAutor = findViewById(R.id.display_archivo_boton_descargar_no_autor);
        botonDescargarAutor = findViewById(R.id.display_archivo_boton_descargar_autor);
        botonEditarNombre = findViewById(R.id.button_edit_nombre);
        botonBorrar = findViewById(R.id.button_delete);

        if (idArchivo != -1) {
            loadArchivo(idArchivo);
        }

        bottomNavigationView.getMenu().getItem(menuPosition).setChecked(true);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(DisplayArchivo.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(DisplayArchivo.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    Intent intentApuntes = new Intent(DisplayArchivo.this, Apuntes.class);
                    intentApuntes.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentApuntes);
                    finish();
                    return true;
                case R.id.navigation_subir:
                    Intent intentUploadFile= new Intent(DisplayArchivo.this, UploadFile.class);
                    intentUploadFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUploadFile);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(DisplayArchivo.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });
    }

    /**
     * Aquí se llama a la base de datos para obtener el archivo a mostrar. También se encarga de rellenar los elementos de la UI con los datos que obtiene.
     * @param idArchivo identificador del archivo a mostrar
     */
    private void loadArchivo(int idArchivo) {
        archivosRef.whereEqualTo("id", idArchivo).get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (!queryDocumentSnapshots.getDocuments().isEmpty()) {
                archivo = queryDocumentSnapshots.getDocuments().get(0).toObject(Archivo.class);

                if (settings.getString(SessionConstants.USERNAME, "InvalidUser").equals(archivo.getAutor())) {
                    botonEditarNombre.setVisibility(View.VISIBLE);
                    botonBorrar.setVisibility(View.VISIBLE);
                    botonDescargarAutor.setVisibility(View.VISIBLE);
                    tamanioAutor.setVisibility(View.VISIBLE);
                    botonDescargarNoAutor.setVisibility(View.GONE);
                    tamanioNoAutor.setVisibility(View.GONE);

                    ViewGroup.MarginLayoutParams paramsBorrar = (ViewGroup.MarginLayoutParams) botonBorrar.getLayoutParams();
                    paramsBorrar.rightMargin = 120;
                    botonBorrar.setLayoutParams(paramsBorrar);

                    ViewGroup.MarginLayoutParams paramsDescargar = (ViewGroup.MarginLayoutParams) botonDescargarAutor.getLayoutParams();
                    paramsDescargar.rightMargin = 210;
                    botonDescargarAutor.setLayoutParams(paramsDescargar);

                    botonBorrar.setOnClickListener(v -> {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DisplayArchivo.this);
                        builder.setCancelable(true);
                        builder.setTitle("Borrar " + archivo.getNombreArchivo());
                        builder.setMessage("¿Seguro que quieres borrar el archivo?");
                        builder.setPositiveButton("BORRAR", (dialog, which) -> {
                            storageReference.child(archivo.getId() + "." + archivo.getExtension()).delete();
                            novedadesArchivosRef.whereEqualTo("idArchivo", archivo.getId()).get().addOnSuccessListener(novedadArchivoQueryDocumentSnapshots -> {
                                if (!novedadArchivoQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                    novedadArchivoQueryDocumentSnapshots.getDocuments().get(0).getReference().delete();
                                }
                            });
                            queryDocumentSnapshots.getDocuments().get(0).getReference().delete();
                            setResult(Activity.RESULT_OK);
                            Intent intent = new Intent(context, Inicio.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(intent);
                        });
                        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    });

                    botonEditarNombre.setOnClickListener(v -> {
                        AlertDialog.Builder alert = new AlertDialog.Builder(context);
                        alert.setCancelable(true);
                        alert.setTitle("Editar nombre");
                        alert.setMessage("Nuevo nombre del archivo");

                        // Set an EditText view to get user input
                        final EditText input = new EditText(context);
                        alert.setView(input);

                        alert.setPositiveButton("ACEPTAR", (dialog, whichButton) -> {
                            boolean vacio = (input.getText().toString().trim().isEmpty());
                            if (vacio) {
                                dialog.dismiss();
                                Toast.makeText(DisplayArchivo.this, "El nombre no puede estar vacío", Toast.LENGTH_LONG).show();
                            } else {
                                String nuevoNombre = input.getText().toString();

                                Map<String, Object> data = new HashMap<>();
                                data.put("nombreArchivo", nuevoNombre);
                                queryDocumentSnapshots.getDocuments().get(0).getReference().set(data, SetOptions.merge());

                                archivo.setNombreArchivo(nuevoNombre);
                                nombre.setText(nuevoNombre);
                            }
                        });

                        alert.setNegativeButton("CANCELAR", (dialog, whichButton) -> {
                        });
                        AlertDialog dialog = alert.create();
                        alert.show();
                    });
                }

                String nombreArchivo = archivo.getNombreArchivo();
                String extensionArchivo = archivo.getExtension();

                nombre.setText(archivo.getNombreArchivo());
                tamanioNoAutor.setText("(" + unit(archivo.getTamano()) + ")");
                tamanioAutor.setText("(" + unit(archivo.getTamano()) + ")");

                asignaturasRef.whereEqualTo("id", archivo.getAsignatura()).get().addOnSuccessListener(queryAsignaturaDocumentSnapshots -> {
                    if (!queryAsignaturaDocumentSnapshots.getDocuments().isEmpty()) {
                        Asignatura asignaturaEntity = queryAsignaturaDocumentSnapshots.getDocuments().get(0).toObject(Asignatura.class);

                        SpannableString asignaturaSpan = Links.crearSpan(asignaturaEntity.getNombre(), v -> {
                            Intent intent = new Intent(context, ArchivosAsignatura.class);
                            intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
                            intent.putExtra(SessionConstants.EXTRA_ID, asignaturaEntity.getId());
                            context.startActivity(intent);
                        }, false, 0xFF1F13D3);
                        asignatura.setText(asignaturaSpan);
                        Links.makeLinksFocusable(asignatura);
                    }
                });
                SpannableString autorSpan = Links.crearSpan(archivo.getAutor(), v -> {
                    Intent intentPerfil = new Intent(context, DisplayProfile.class);
                    intentPerfil.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, archivo.getAutor());
                    context.startActivity(intentPerfil);
                }, false, 0xFF1F13D3);
                autor.setText(autorSpan);
                Links.makeLinksFocusable(autor);
                String fechaText = archivo.getFecha().substring(8, 10) + "/" + archivo.getFecha().substring(5, 7) + "/" + archivo.getFecha().substring(0, 4) + " - " + archivo.getFecha().substring(11);
                fecha.setText(fechaText);

                storageReference.child(archivo.getId() + "." + archivo.getExtension()).getDownloadUrl()
                        .addOnSuccessListener(uri -> Picasso.get().load(uri).into(imagen));

                botonDescargarNoAutor.setOnClickListener(v -> descargarArchivo());
                botonDescargarAutor.setOnClickListener(v -> descargarArchivo());
            }
        });
    }

    /**
     * Este método se utiliza para mostrar el tamaño del archivo en sus unidades correspondientes
     * @param size tamaño del archivo
     * @return string con el tamaño de su archivo en su unidad correspondiente
     */
    private String unit(int size){
        String[] units = {"B", "KB", "MB", "GB"};
        int original = size;
        int unit = 0;
        while (size >= 1) {
            size = size / 1024;
            unit++;
        }
        if(unit == 0)
            unit++;
        DecimalFormat df2 = new DecimalFormat("#");
        double numero = original / Math.pow(1024, unit - 1);
        return df2.format(numero) + " " + units[unit - 1];
    }

    /**
     * Este método llama al que se encarga de descargar el archivo una vez se ha comprobado que los permisos están otorgados.
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case SessionConstants.PERMISSION_STORAGE_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permiso otorgado por el popup, realizar descarga
                    descargarArchivo();
                } else {
                    // permiso denegado por el popup, mostrar mensaje de error
                    Toast.makeText(context, "Permission denied...!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    /**
     * Este método permite al usuario descargar el archivo que se está mostrando.
     */
    private void descargarArchivo() {
        storageReference.child(archivo.getId() + "." + archivo.getExtension()).getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    DownloadManager.Request request = new DownloadManager.Request(uri);
                    // permitir tipos de red para descargar archivos
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                    // nombre del archivo
                    request.setTitle(archivo.getNombreArchivo() + "." + archivo.getExtension());
                    // establecer descripción en la notificación de descarga
                    request.setDescription("Downloading file...");

                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    // establecer path donde guardar la descarga
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, archivo.getNombreArchivo() + "." + archivo.getExtension());

                    // coger el servicio de descarga y encolar el archivo a descargar
                    DownloadManager manager = (DownloadManager)this.getSystemService(Context.DOWNLOAD_SERVICE);
                    manager.enqueue(request);


                });
    }
}