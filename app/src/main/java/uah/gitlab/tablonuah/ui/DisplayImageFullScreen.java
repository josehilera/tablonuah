package uah.gitlab.tablonuah.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Usuario;
import uah.gitlab.tablonuah.session.SessionConstants;

public class DisplayImageFullScreen extends AppCompatActivity {

    ImageView imagen;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference archivosRef = db.collection("Archivo");
    private CollectionReference usuariosRef = db.collection("Usuario");
    private StorageReference archivoStorageReference;
    private StorageReference perfilStorageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_display_archivo_full_screen);

        imagen = findViewById(R.id.display_imagen_full_screen);

        imagen.setOnClickListener(view -> {
            finish();
            /*int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
            int newUiOptions = uiOptions;

            boolean isImmersiveModeEnabled = ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);

            if (Build.VERSION.SDK_INT >= 14) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            }

            if (Build.VERSION.SDK_INT >= 16) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
            }

            if (Build.VERSION.SDK_INT >= 18) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }

            getWindow().getDecorView().setSystemUiVisibility(newUiOptions);*/
        });

        Intent intent = getIntent();
        int idArchivo = intent.getIntExtra(SessionConstants.EXTRA_ID, -1);
        String idUsuario = intent.getStringExtra(SessionConstants.EXTRA_ID_USUARIO);
        String tipoImagen = intent.getStringExtra(SessionConstants.EXTRA_TIPO_IMAGEN);

        archivoStorageReference = FirebaseStorage.getInstance().getReference("Archivo");
        perfilStorageReference = FirebaseStorage.getInstance().getReference("Perfil");

        if (tipoImagen.equals("ARCHIVO")) {
            archivosRef.whereEqualTo("id", idArchivo).get().addOnSuccessListener(queryDocumentSnapshots -> {
                if (!queryDocumentSnapshots.getDocuments().isEmpty()) {
                    Archivo archivo = queryDocumentSnapshots.getDocuments().get(0).toObject(Archivo.class);

                    archivoStorageReference.child(archivo.getId() + "." + archivo.getExtension()).getDownloadUrl()
                            .addOnSuccessListener(uri -> Picasso.get().load(uri).into(imagen));
                }
            });
        } else if (tipoImagen.equals("PERFIL")) {
            usuariosRef.whereEqualTo("nombreUsuario", idUsuario).get().addOnSuccessListener(usuarioQueryDocumentSnapshots -> {
                if (!usuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                    Usuario usuario = usuarioQueryDocumentSnapshots.getDocuments().get(0).toObject(Usuario.class);

                    perfilStorageReference.child(usuario.getNombreUsuario() + "." + usuario.getExtensionImagen()).getDownloadUrl()
                            .addOnSuccessListener(uri -> Picasso.get().load(uri).into(imagen));
                }
            });
        }
    }

}