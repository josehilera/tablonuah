package uah.gitlab.tablonuah.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Usuario;
import uah.gitlab.tablonuah.session.SessionConstants;

public class DisplayProfile extends AppCompatActivity {

    private static final int MENU_POSITION_PERFIL_PROPIO = 4;
    private static final String TEST_USER_GITLAB_AND_GOOGLE = "alvlop18";
    private static final String TEST_USER_LOCAL = "alvlop20";

    private static int menuPosition = -1;

    private static final int PICK_IMAGE_REQUEST = 1;

    private StorageTask uploadTask;
    private Uri imageUri;

    BottomNavigationView bottomNavigationView;

    SharedPreferences settings;

    Usuario usuario;

    TextView noArchivos;
    TextView noArchivosAjeno;
    TextView tituloArchivo;
    TextView tituloAsignatura;
    TextView tituloCurso;

    private ProgressBar progressBar;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference archivosRef = db.collection("Archivo");
    private CollectionReference novedadesArchivosRef = db.collection("NovedadArchivo");
    private CollectionReference asignaturasMarcadasRef = db.collection("AsignaturaMarcada");
    private CollectionReference usuariosRef = db.collection("Usuario");
    private StorageReference perfilStorageReference;
    private StorageReference archivoStorageReference;

    private AdapterArchivosUsuario adapter;

    RecyclerView recyclerView;

    TextView nombre;
    TextView archivos_subidos;
    ImageView imagen;
    ImageButton botonEditarNombre;
    ImageButton botonEditarImagen;

    Button botonEditarPassword;
    Button botonBorrarCuenta;
    Button botonCerrarSesion;
    ImageButton botonBorrarImagen;

    String idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_profile);

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        perfilStorageReference = FirebaseStorage.getInstance().getReference("Perfil");
        archivoStorageReference = FirebaseStorage.getInstance().getReference("Archivo");

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        idUsuario = intent.getStringExtra(SessionConstants.EXTRA_ID);
        menuPosition = intent.getIntExtra(SessionConstants.EXTRA_MENU_POSITION, -1);

        archivos_subidos = findViewById(R.id.display_usuario_archivos_subidos);
        archivos_subidos.setText("" + 0);

        recyclerView = findViewById(R.id.archivos_usuario_profile_recycler_view);

        setUpRecyclerView();
        if (idUsuario.equals(settings.getString(SessionConstants.USERNAME, "InvalidUser"))) {
            archivos_subidos.setVisibility(View.GONE);
            findViewById(R.id.display_usuario_archivos_subidos_titulo).setVisibility(View.GONE);
        }

        nombre = findViewById(R.id.display_usuario_nombre);
        imagen = findViewById(R.id.display_usuario_imagen);
        botonEditarNombre = findViewById(R.id.button_edit_nombre_usuario);
        botonEditarImagen = findViewById(R.id.button_edit_imagen_usuario);
        botonEditarPassword = findViewById(R.id.boton_edit_password);
        botonBorrarCuenta = findViewById(R.id.boton_borrar_cuenta);
        botonCerrarSesion = findViewById(R.id.boton_cerrar_sesion);
        botonBorrarImagen = findViewById(R.id.button_delete_imagen_usuario);

        noArchivos = findViewById(R.id.no_archivos);
        noArchivosAjeno = findViewById(R.id.no_archivos_ajeno);

        recyclerView.setVisibility(View.GONE);

        noArchivosAjeno.setVisibility(View.GONE);
        noArchivos.setVisibility(View.GONE);

        progressBar = findViewById(R.id.progress_bar);

        progressBar.setVisibility(View.INVISIBLE);

        tituloArchivo = findViewById(R.id.titulo_archivo);
        tituloAsignatura = findViewById(R.id.titulo_asignatura);
        tituloCurso = findViewById(R.id.titulo_curso);

        tituloArchivo.setVisibility(View.GONE);
        tituloAsignatura.setVisibility(View.GONE);
        tituloCurso.setVisibility(View.GONE);

        loadUsuario();

        if (idUsuario.equals(settings.getString(SessionConstants.USERNAME, "InvalidUser")) && menuPosition == -1) {
            bottomNavigationView.getMenu().getItem(MENU_POSITION_PERFIL_PROPIO).setChecked(true);
        }
        else {
            bottomNavigationView.getMenu().getItem(menuPosition).setChecked(true);
        }

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(DisplayProfile.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(DisplayProfile.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    Intent intentApuntes = new Intent(DisplayProfile.this, Apuntes.class);
                    intentApuntes.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentApuntes);
                    finish();
                    return true;
                case R.id.navigation_subir:
                    Intent intentSubir = new Intent(DisplayProfile.this, UploadFile.class);
                    intentSubir.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentSubir);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    if (!idUsuario.equals(settings.getString(SessionConstants.USERNAME, "InvalidUser"))) {
                        Intent intentPerfil = new Intent(DisplayProfile.this, DisplayProfile.class);
                        intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                        startActivity(intentPerfil);
                        finish();
                    }
                    return true;
            }
            return false;
        });

        checkArchivosUsuarioExistance();
    }

    private void setUpRecyclerView() {
        Query query = archivosRef.whereEqualTo("autor", idUsuario);

        FirestoreRecyclerOptions<Archivo> options = new FirestoreRecyclerOptions.Builder<Archivo>()
                .setQuery(query, Archivo.class)
                .build();

        adapter = new AdapterArchivosUsuario(options, this, menuPosition);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(null);

        /* implementado con un span en el onBindViewHolder del adapter
        adapter.setOnFileTitleClickListener((documentSnapshot, position) -> {
            int idArchivo = archivosList.get(position).getId();

            Intent intent = new Intent(context, DisplayArchivo.class);
            intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, this.menuPosition);
            intent.putExtra(SessionConstants.EXTRA_ID, idArchivo);
            startActivity(intent);
        });*/
    }

    private void checkArchivosUsuarioExistance() {
        if (!idUsuario.equals(settings.getString(SessionConstants.USERNAME, "InvalidUser"))) {
            archivosRef.whereEqualTo("autor", idUsuario).get().addOnSuccessListener(archivosUsuarioQueryDocumentSnapshots -> {
                archivos_subidos.setText("" + archivosUsuarioQueryDocumentSnapshots.getDocuments().size());

                if (archivosUsuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                    recyclerView.setVisibility(View.GONE);
                    tituloArchivo.setVisibility(View.GONE);
                    tituloAsignatura.setVisibility(View.GONE);
                    tituloCurso.setVisibility(View.GONE);

                    if (idUsuario.equals(settings.getString(SessionConstants.USERNAME, "InvalidUser"))) {
                        noArchivos.setVisibility(View.VISIBLE);
                    }
                    else {
                        noArchivosAjeno.setVisibility(View.VISIBLE);
                    }
                }
                else {
                    recyclerView.setVisibility(View.VISIBLE);
                    tituloArchivo.setVisibility(View.VISIBLE);
                    tituloAsignatura.setVisibility(View.VISIBLE);
                    tituloCurso.setVisibility(View.VISIBLE);

                    noArchivosAjeno.setVisibility(View.GONE);
                    noArchivos.setVisibility(View.GONE);
                }
            });
        }
    }

    /**
     * Aquí se llama a la base de datos para obtener el usuario y rellenar la página con los datos obtenidos.
     */
    private void loadUsuario() {
        usuariosRef.whereEqualTo("nombreUsuario", idUsuario).get().addOnSuccessListener(usuarioQueryDocumentSnapshots -> {
            if (!usuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                usuario = usuarioQueryDocumentSnapshots.getDocuments().get(0).toObject(Usuario.class);

                nombre.setText(usuario.getNombreUsuario());

                if (usuario.getExtensionImagen().equals("jpg") || usuario.getExtensionImagen().equals("jpeg") || usuario.getExtensionImagen().equals("png")) {
                    perfilStorageReference.child(usuario.getNombreUsuario() + "." + usuario.getExtensionImagen()).getDownloadUrl()
                            .addOnSuccessListener(uri -> {
                                Picasso.get().load(uri).into(imagen);
                                imagen.setOnClickListener(view -> {
                                    Intent intentFullScreenImage = new Intent(this, DisplayImageFullScreen.class);
                                    intentFullScreenImage.putExtra(SessionConstants.EXTRA_ID_USUARIO, idUsuario);
                                    intentFullScreenImage.putExtra(SessionConstants.EXTRA_TIPO_IMAGEN, "PERFIL");
                                    this.startActivity(intentFullScreenImage);
                                });
                            });
                } else {
                    Picasso.get().load(R.drawable.img_profile_logo).into(imagen);
                }

                if (settings.getString(SessionConstants.USERNAME, "InvalidUser").equals(usuario.getNombreUsuario())) {
                    botonEditarNombre.setVisibility(View.VISIBLE);
                    botonEditarImagen.setVisibility(View.VISIBLE);
                    botonEditarPassword.setVisibility(View.VISIBLE);
                    botonBorrarCuenta.setVisibility(View.VISIBLE);
                    botonCerrarSesion.setVisibility(View.VISIBLE);
                    if (!usuario.getExtensionImagen().equals("none")) {
                        botonBorrarImagen.setVisibility(View.VISIBLE);
                    }

                    botonEditarImagen.setOnClickListener(v -> {
                        filePicker();
                    });

                    botonCerrarSesion.setOnClickListener(v -> {
                        Intent intentLogOut = new Intent(this, LogIn.class);
                        startActivity(intentLogOut);
                        finish();
                    });

                    botonBorrarCuenta.setOnClickListener(v -> {
                        if (usuario.getNombreUsuario().equals(TEST_USER_GITLAB_AND_GOOGLE) || usuario.getNombreUsuario().equals(TEST_USER_LOCAL)) {
                            Toast.makeText(this, "Este usuario no puede ser borrado", Toast.LENGTH_LONG).show();
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setCancelable(true);
                            builder.setTitle("Borrar cuenta");
                            builder.setMessage("¿Seguro que quieres borrar tu cuenta?\n\nSe borrarán tus datos y contribuciones.");
                            builder.setPositiveButton("BORRAR",
                                    (dialog, which) -> {
                                        if (!usuario.getExtensionImagen().equals("none")) {
                                            perfilStorageReference.child(usuario.getNombreUsuario() + "." + usuario.getExtensionImagen()).delete();
                                        }

                                        archivosRef.whereEqualTo("autor", idUsuario).get().addOnSuccessListener(archivosUsuarioQueryDocumentSnapshots -> {
                                            if (!archivosUsuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                                for (DocumentSnapshot documentSnapshot : archivosUsuarioQueryDocumentSnapshots.getDocuments()) {
                                                    Archivo archivo = documentSnapshot.toObject(Archivo.class);
                                                    documentSnapshot.getReference().delete();
                                                    archivoStorageReference.child(archivo.getId() + "." + archivo.getExtension()).delete();
                                                }
                                            }
                                        });

                                        usuarioQueryDocumentSnapshots.getDocuments().get(0).getReference().delete();

                                        asignaturasMarcadasRef.whereEqualTo("idUsuario", idUsuario).get().addOnSuccessListener(asignaturasMarcadasUsuarioQueryDocumentSnapshots -> {
                                            if (!asignaturasMarcadasUsuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                                for (DocumentSnapshot documentSnapshot : asignaturasMarcadasUsuarioQueryDocumentSnapshots.getDocuments()) {
                                                    documentSnapshot.getReference().delete();
                                                }
                                            }
                                        });

                                        novedadesArchivosRef.whereEqualTo("usuarioEmisor", idUsuario).get().addOnSuccessListener(novedadesArchivosUsuarioQueryDocumentSnapshots -> {
                                            if (!novedadesArchivosUsuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                                for (DocumentSnapshot documentSnapshot : novedadesArchivosUsuarioQueryDocumentSnapshots.getDocuments()) {
                                                    documentSnapshot.getReference().delete();
                                                }
                                            }
                                        });

                                        Toast.makeText(this, "Sesión cerrada", Toast.LENGTH_SHORT).show();

                                        setResult(Activity.RESULT_OK);
                                        settings.edit().putString(SessionConstants.USERNAME, "").apply();
                                        Intent intentLogOut = new Intent(this, LogIn.class);
                                        startActivity(intentLogOut);
                                        finish();
                                    });
                            builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });

                    botonBorrarImagen.setOnClickListener(v -> {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setCancelable(true);
                        builder.setTitle("Borrar imagen");
                        builder.setMessage("¿Seguro que quieres borrar tu imagen?");
                        builder.setPositiveButton("BORRAR",
                                (dialog, which) -> {
                                    imagen.setOnClickListener(null);

                                    perfilStorageReference.child(usuario.getNombreUsuario() + "." + usuario.getExtensionImagen()).delete();

                                    Map<String, Object> data = new HashMap<>();
                                    data.put("extensionImagen", "none");
                                    usuarioQueryDocumentSnapshots.getDocuments().get(0).getReference().set(data, SetOptions.merge());

                                    usuario.setExtensionImagen("none");

                                    Picasso.get().load(R.drawable.img_profile_logo).into(imagen);

                                    botonBorrarImagen.setVisibility(View.GONE);

                                    Toast.makeText(DisplayProfile.this, "Imagen borrada", Toast.LENGTH_SHORT).show();
                                });
                        builder.setNegativeButton("CANCELAR", (dialog, which) -> {
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    });

                    botonEditarPassword.setOnClickListener(v -> {
                        if (usuario.getNombreUsuario().equals(TEST_USER_GITLAB_AND_GOOGLE) || usuario.getNombreUsuario().equals(TEST_USER_LOCAL)) {
                            Toast.makeText(this, "Este usuario no puede cambiar la contraseña", Toast.LENGTH_LONG).show();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(this);
                            alert.setCancelable(true);
                            alert.setTitle("Editar contraseña");
                            alert.setMessage("Nueva contraseña");

                            // Set an EditText view to get user input
                            final EditText input = new EditText(this);
                            input.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            alert.setView(input);

                            alert.setPositiveButton("ACEPTAR", (dialog, whichButton) -> {
                                boolean vacio = (input.getText().toString().isEmpty());
                                if (vacio) {
                                    dialog.dismiss();
                                    Toast.makeText(DisplayProfile.this, "La contraseña no puede estar vacía", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    String nuevaPassword = input.getText().toString();
                                    Map<String, Object> data = new HashMap<>();
                                    data.put("password", nuevaPassword);
                                    usuarioQueryDocumentSnapshots.getDocuments().get(0).getReference().set(data, SetOptions.merge());
                                    usuario.setPassword(nuevaPassword);
                                    Toast.makeText(DisplayProfile.this, "Contraseña actualizada", Toast.LENGTH_SHORT).show();
                                }
                            });

                            alert.setNegativeButton("CANCELAR", (dialog, whichButton) -> {});
                            AlertDialog dialog = alert.create();
                            dialog.show();
                        }
                    });

                    botonEditarNombre.setOnClickListener(v -> {
                        if (usuario.getNombreUsuario().equals(TEST_USER_GITLAB_AND_GOOGLE) || usuario.getNombreUsuario().equals(TEST_USER_LOCAL)) {
                            Toast.makeText(this, "Este usuario no puede cambiar la contraseña", Toast.LENGTH_LONG).show();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(this);
                            alert.setCancelable(true);
                            alert.setTitle("Editar nombre de usuario");
                            alert.setMessage("Nuevo nombre de usuario");

                            // Set an EditText view to get user input
                            final EditText input = new EditText(this);
                            alert.setView(input);

                            alert.setPositiveButton("ACEPTAR", (dialog, whichButton) -> {
                                boolean vacio = (input.getText().toString().trim().isEmpty());
                                if (vacio) {
                                    dialog.dismiss();
                                    Toast.makeText(DisplayProfile.this, "El nombre no puede estar vacío", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    String nuevoNombre = input.getText().toString().replace("\n", "");
                                    if (nuevoNombre.equals(idUsuario)) {
                                        Toast.makeText(DisplayProfile.this, "Ya tienes ese nombre de usuario", Toast.LENGTH_SHORT).show();
                                    } else {
                                        usuariosRef.whereEqualTo("nombreUsuario", nuevoNombre).get().addOnSuccessListener(nuevoUsuarioQueryDocumentSnapshots -> {
                                            if (!nuevoUsuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                                Toast.makeText(DisplayProfile.this, "Ya existe otro usuario con ese nombre", Toast.LENGTH_SHORT).show();
                                            } else {
                                                String antiguoNombreUsuario = idUsuario;

                                                Map<String, Object> data = new HashMap<>();
                                                data.put("nombreUsuario", nuevoNombre);
                                                usuarioQueryDocumentSnapshots.getDocuments().get(0).getReference().set(data, SetOptions.merge());

                                                archivosRef.whereEqualTo("autor", antiguoNombreUsuario).get().addOnSuccessListener(archivosUsuarioQueryDocumentSnapshots -> {
                                                    if (!archivosUsuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                                        for (DocumentSnapshot archivoDocumentSnapshot : archivosUsuarioQueryDocumentSnapshots.getDocuments()) {
                                                            Map<String, Object> dataArchivo = new HashMap<>();
                                                            dataArchivo.put("autor", nuevoNombre);
                                                            archivoDocumentSnapshot.getReference().set(dataArchivo, SetOptions.merge());
                                                        }
                                                    }
                                                });
                                                novedadesArchivosRef.whereEqualTo("usuarioEmisor", antiguoNombreUsuario).get().addOnSuccessListener(novedadesArchivosUsuarioQueryDocumentSnapshots -> {
                                                    if (!novedadesArchivosUsuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                                        for (DocumentSnapshot novedadArchivoDocumentSnapshot : novedadesArchivosUsuarioQueryDocumentSnapshots.getDocuments()) {
                                                            Map<String, Object> dataNovedadArchivo = new HashMap<>();
                                                            dataNovedadArchivo.put("usuarioEmisor", nuevoNombre);
                                                            novedadArchivoDocumentSnapshot.getReference().set(dataNovedadArchivo, SetOptions.merge());
                                                        }
                                                    }
                                                });
                                                asignaturasMarcadasRef.whereEqualTo("idUsuario", antiguoNombreUsuario).get().addOnSuccessListener(asignaturasMarcadasUsuarioQueryDocumentSnapshots -> {
                                                    if (!asignaturasMarcadasUsuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                                                        for (DocumentSnapshot asignaturaMarcadaDocumentSnapshot : asignaturasMarcadasUsuarioQueryDocumentSnapshots.getDocuments()) {
                                                            Map<String, Object> dataAsignaturaMarcada = new HashMap<>();
                                                            dataAsignaturaMarcada.put("idUsuario", nuevoNombre);
                                                            asignaturaMarcadaDocumentSnapshot.getReference().set(dataAsignaturaMarcada, SetOptions.merge());
                                                        }
                                                    }
                                                });

                                                if (!usuario.getExtensionImagen().equals("none")) {
                                                    perfilStorageReference.child(antiguoNombreUsuario + "." + usuario.getExtensionImagen()).getDownloadUrl().addOnSuccessListener(uri -> {
                                                        StorageReference fileReference = perfilStorageReference.child(nuevoNombre + "." + usuario.getExtensionImagen());
                                                        imagen.setDrawingCacheEnabled(true);
                                                        imagen.buildDrawingCache();
                                                        Bitmap bitmap = ((BitmapDrawable) imagen.getDrawable()).getBitmap();
                                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                                        Bitmap.CompressFormat format = null;
                                                        if (usuario.getExtensionImagen().equals("jpg") || usuario.getExtensionImagen().equals("jpeg")) {
                                                            format = Bitmap.CompressFormat.JPEG;
                                                        } else if (usuario.getExtensionImagen().equals("png")) {
                                                            format = Bitmap.CompressFormat.PNG;
                                                        } else if (usuario.getExtensionImagen().equals("webp")) {
                                                            format = Bitmap.CompressFormat.WEBP;
                                                        }
                                                        bitmap.compress(format, 100, baos);
                                                        byte[] byteArrayData = baos.toByteArray();

                                                        uploadTask = fileReference.putBytes(byteArrayData).addOnSuccessListener(taskSnapshot -> {

                                                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                                                            while (!urlTask.isSuccessful());

                                                            perfilStorageReference.child(antiguoNombreUsuario + "." + usuario.getExtensionImagen()).delete();

                                                        }).addOnFailureListener(e -> Toast.makeText(DisplayProfile.this, e.getMessage(), Toast.LENGTH_SHORT).show());
                                                    });
                                                }

                                                idUsuario = nuevoNombre;
                                                usuario.setNombreUsuario(nuevoNombre);
                                                nombre.setText(nuevoNombre);
                                                settings.edit().putString(SessionConstants.USERNAME, nuevoNombre).apply();

                                                Toast.makeText(DisplayProfile.this, "Nombre de usuario actualizado", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }
                            });

                            alert.setNegativeButton("CANCELAR", (dialog, whichButton) -> {});
                            AlertDialog dialog = alert.create();
                            dialog.show();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkArchivosUsuarioExistance();
    }

    @Override
    public void onBackPressed() {
        if (menuPosition != -1) {
            super.onBackPressed();
        }
    }

    /**
     * Este método se utiliza para poder seleccionar el archivo a subir. Actualmente solo puede ser una imagen.
     */
    private void filePicker() {
        String[] mimeTypes = {"image/jpeg", "image/png", "image/webp"};
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            imageUri = data.getData();

            if (uploadTask != null && uploadTask.isInProgress()) {
                Toast.makeText(DisplayProfile.this, "Subida en progreso", Toast.LENGTH_SHORT).show();
            }
            else {
                if (imageUri != null) {
                    usuariosRef.whereEqualTo("nombreUsuario", idUsuario).get().addOnSuccessListener(usuarioQueryDocumentSnapshots -> {
                        if (!usuarioQueryDocumentSnapshots.getDocuments().isEmpty()) {
                            progressBar.setVisibility(View.VISIBLE);

                            if (!usuario.getExtensionImagen().equals("none")) {
                                perfilStorageReference.child(idUsuario + "." + usuario.getExtensionImagen()).delete();
                            }

                            StorageReference fileReference = perfilStorageReference.child(idUsuario + "." + getFileExtension(imageUri));
                            uploadTask = fileReference.putFile(imageUri).addOnSuccessListener(taskSnapshot -> {

                                Handler handler = new Handler();
                                handler.postDelayed(() -> progressBar.setProgress(0), 500);

                                Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                                while (!urlTask.isSuccessful());
                                //Uri downloadUrl = urlTask.getResult();

                                //Log.d(TAG, "onSuccess: firebase download url: " + downloadUrl.toString()); //use if testing...don't need this line.


                                Map<String, Object> extensionImagenData = new HashMap<>();
                                extensionImagenData.put("extensionImagen", getFileExtension(imageUri));
                                usuarioQueryDocumentSnapshots.getDocuments().get(0).getReference().set(extensionImagenData, SetOptions.merge());

                                usuario.setExtensionImagen(getFileExtension(imageUri));

                                Picasso.get().load(imageUri).into(imagen);

                                imagen.setOnClickListener(view -> {
                                    Intent intentFullScreenImage = new Intent(this, DisplayImageFullScreen.class);
                                    intentFullScreenImage.putExtra(SessionConstants.EXTRA_ID_USUARIO, idUsuario);
                                    intentFullScreenImage.putExtra(SessionConstants.EXTRA_TIPO_IMAGEN, "PERFIL");
                                    this.startActivity(intentFullScreenImage);
                                });

                                botonBorrarImagen.setVisibility(View.VISIBLE);

                                Toast.makeText(DisplayProfile.this, "Imagen actualizada correctamente", Toast.LENGTH_SHORT).show();

                                progressBar.setVisibility(View.INVISIBLE);

                            }).addOnFailureListener(e -> Toast.makeText(DisplayProfile.this, e.getMessage(), Toast.LENGTH_SHORT).show())
                                    .addOnProgressListener(snapshot -> {
                                        double progress = (100.0 * snapshot.getBytesTransferred() / snapshot.getTotalByteCount());
                                        progressBar.setProgress((int) progress);
                                    });
                        }
                    });
                } else {
                    Toast.makeText(this, "Selecciona una imagen", Toast.LENGTH_LONG).show();
                }
            }
        }
        else {
            Toast.makeText(this, "Selecciona una imagen", Toast.LENGTH_LONG).show();
        }
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        adapter.stopListening();
    }

}