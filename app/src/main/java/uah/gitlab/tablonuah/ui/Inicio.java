package uah.gitlab.tablonuah.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.text.LineBreaker;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.preference.PreferenceManager;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.AsignaturaMarcada;
import uah.gitlab.tablonuah.model.Note;
import uah.gitlab.tablonuah.model.NovedadArchivo;
import uah.gitlab.tablonuah.session.SessionConstants;

public class Inicio extends AppCompatActivity {

    private static final int MENU_POSITION = 0;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference novedadesArchivosRef = db.collection("NovedadArchivo");
    private CollectionReference asignaturasMarcadasRef = db.collection("AsignaturaMarcada");
    List<Integer> listaIdsAsignaturasMarcadas = new ArrayList<Integer>();
    List<AsignaturaMarcada> asignaturasMarcadasList = new ArrayList<AsignaturaMarcada>();

    private AdapterNovedadArchivo adapter;

    SharedPreferences settings;

    TextView noNovedades;

    RecyclerView recyclerView;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        setUpRecyclerView();

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        noNovedades = findViewById(R.id.no_novedades);

        loadAsignaturasMarcadas();

        bottomNavigationView.getMenu().getItem(MENU_POSITION).setChecked(true);

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(Inicio.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    Intent intentApuntes = new Intent(Inicio.this, Apuntes.class);
                    intentApuntes.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentApuntes);
                    finish();
                    return true;
                case R.id.navigation_subir:
                    Intent intentUploadFile = new Intent(Inicio.this, UploadFile.class);
                    intentUploadFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUploadFile);
                    finish();
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(Inicio.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });
    }

    private void setUpRecyclerView() {
        Query query = novedadesArchivosRef.orderBy("fecha", Query.Direction.DESCENDING);

        FirestoreRecyclerOptions<NovedadArchivo> options = new FirestoreRecyclerOptions.Builder<NovedadArchivo>()
                .setQuery(query, NovedadArchivo.class)
                .build();

        adapter = new AdapterNovedadArchivo(options, this, MENU_POSITION);

        recyclerView = findViewById(R.id.inicio_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        /*
             // no hay forma de borrar el elemento y que se actualice su posición (se muevan arriba las de abajo del borrado)
             // sin borrar realmente el elemento de la base de datos, pero las novedades son generales y todos tienen acceso a ellas,
             // por lo que si alguien la borra, se borrará para todos los usuarios
             // por ello, deshabilitamos directamente la posibilidad de hacer swipe
                // la única alternativa sería implementar una forma de almacenar las notificaciones que tiene cada usuario
                // (o más bien las que un usuario ya ha borrado, y comprobar si el usuario la había borrado antes de mostrarla)
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                // no borramos la novedad porque es una novedad global que se enseña a todos los usuarios, solo se la ocultamos al usuario
                adapter.deleteItem(viewHolder.getAbsoluteAdapterPosition());

                if (recyclerView.getAdapter().getItemCount() == 1) {
                    recyclerView.setVisibility(View.GONE);
                    noNovedades.setVisibility(View.VISIBLE);
                }
            }
        }).attachToRecyclerView(recyclerView);*/

        adapter.setOnTitleClickListener(new AdapterNovedadArchivo.OnTitleClickListener() {
            @Override
            public void onTitleClick(DocumentSnapshot documentSnapshot, int position) {
                Note note = documentSnapshot.toObject(Note.class);
                String id = documentSnapshot.getId();

                //notebook/56sdfSFD7653
                String path = documentSnapshot.getReference().getPath();

                //documentSnapshot.getReference().delete();
                //documentSnapshot.getReference().update(...);
                //Toast.makeText(Inicio.this, "Position: " + position + " ID: " + id, Toast.LENGTH_SHORT).show();

                // puedo pasarle a una nueva actividad el id para hacer lo que quiera con ese objeto
                //startActivity();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        adapter.stopListening();
    }

    /**
     * Aquí se queda en la misma página cuando se dé al botón de atrás.
     */
    @Override
    public void onBackPressed() {

    }

    /**
     * Aquí se llama a la base de datos para obtener las asignaturas marcadas por el usuario, para posteriormente obtener las novedades de los archivos pertenecientes a esas asignaturas marcadas.
     */
    private void loadAsignaturasMarcadas() {
        listaIdsAsignaturasMarcadas.clear();
        asignaturasMarcadasList.clear();

        asignaturasMarcadasRef.whereEqualTo("idUsuario", settings.getString(SessionConstants.USERNAME, "InvalidUser")).get().addOnSuccessListener(queryDocumentSnapshots -> {
            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                asignaturasMarcadasList.add(documentSnapshot.toObject(AsignaturaMarcada.class));
            }

            for (AsignaturaMarcada asignaturaMarcada : asignaturasMarcadasList) {
                listaIdsAsignaturasMarcadas.add(asignaturaMarcada.getIdAsignatura());
            }

            if (listaIdsAsignaturasMarcadas.isEmpty()) {
                listaIdsAsignaturasMarcadas.add(-1);
            }

            // Para no mostrar las novedades de archivos del usuario que ha iniciado sesión, hay que añadir lo siguiente
            // .whereNotEqualTo("usuarioEmisor", settings.getString(SessionConstants.USERNAME, "InvalidUser"));
            // pero por limitaciones de Firebase, no se puede aplicar a la vez que .orderBy(fecha),
            // sino que habría que añadir .orderBy(usuarioEmisor) antes del de fecha, haciendo que
            // se muestren primero por orden de usuarioEmisor y luego por orden de fecha dentro del mismo usuarioEmisor

            Query query = novedadesArchivosRef
                    .orderBy("fecha", Query.Direction.ASCENDING)
                    .whereIn("asignatura", listaIdsAsignaturasMarcadas);

            FirestoreRecyclerOptions<NovedadArchivo> options = new FirestoreRecyclerOptions.Builder<NovedadArchivo>()
                    .setQuery(query, NovedadArchivo.class)
                    .build();

            // needed to avid exception after updating options
            recyclerView.setItemAnimator(null);

            adapter.updateOptions(options);

            checkNovedadesExistance();
        });
    }

    private void checkNovedadesExistance() {
        novedadesArchivosRef.get().addOnSuccessListener(novedadesQueryDocumentSnapshots -> {
            recyclerView.setVisibility(View.GONE);
            noNovedades.setVisibility(View.VISIBLE);
            boolean empty = true;
            for (int i = 0; empty && i < novedadesQueryDocumentSnapshots.getDocuments().size(); i++) {
                if (empty && listaIdsAsignaturasMarcadas.contains(novedadesQueryDocumentSnapshots.getDocuments().get(i).toObject(NovedadArchivo.class).getAsignatura())) {
                    recyclerView.setVisibility(View.VISIBLE);
                    noNovedades.setVisibility(View.GONE);
                    empty = false;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadAsignaturasMarcadas();
    }


}