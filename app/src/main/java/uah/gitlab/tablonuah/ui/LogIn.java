package uah.gitlab.tablonuah.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Usuario;
import uah.gitlab.tablonuah.session.SessionConstants;
import uah.gitlab.tablonuah.utils.Links;

/**
 * Actividad para iniciar sesión en la aplicación introduciendo el nombre de usuario y la contraseña.
 */
public class LogIn extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference usuariosRef = db.collection("Usuario");


    EditText nombreUsuario;
    EditText password;
    Button boton_log_in;
    TextView register;

    LifecycleOwner owner;
    Context context;

    SharedPreferences settings;

    List<Usuario> usuariosList = new ArrayList<Usuario>();

    /**
     * Método onCreate en el que se asocian los elementos de la UI y sus correspondientes acciones al interactuar con ellos.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        owner = this;
        context = this;

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        settings.edit().putString(SessionConstants.USERNAME, "").apply();

        //inicializarFirebase();

        loadUsuarios();

        nombreUsuario = findViewById(R.id.edit_text_nombre);
        password = findViewById(R.id.edit_text_contrasenia);
        boton_log_in = findViewById(R.id.button_log_in);
        register = findViewById(R.id.register);
        SpannableString link = Links.crearSpan("Regístrate", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Registro.class);
                startActivity(intent);
            }
        }, true, 0xFF1F13D3);
        register.append(" ");
        register.append(link);
        Links.makeLinksFocusable(register);

        boton_log_in.setOnClickListener(v -> {
            String nombreUsuarioString = nombreUsuario.getText().toString().trim();
            String passwordString = password.getText().toString();
            if (nombreUsuarioString.equalsIgnoreCase("") || passwordString.equalsIgnoreCase("")) {
                if (nombreUsuarioString.equalsIgnoreCase("")) {
                    nombreUsuario.setError("Debes rellenar el nombre de usuario");
                }
                if (passwordString.equalsIgnoreCase("")) {
                    password.setError("Debes rellenar la contraseña");
                }
            } else {
                Usuario usuarioIntroducido = null;
                for (Usuario usuario : usuariosList) {
                    if (usuario.getNombreUsuario().equals(nombreUsuarioString)) {
                        usuarioIntroducido = usuario;
                    }
                }
                if (usuarioIntroducido == null) {
                    Toast.makeText(LogIn.this, "No existe ningún usuario con ese nombre", Toast.LENGTH_LONG).show();
                }
                else {
                    if (passwordString.equals(usuarioIntroducido.getPassword())) {
                        settings.edit().putString(SessionConstants.USERNAME, nombreUsuarioString).apply();

                        Intent intent = new Intent(context, Inicio.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(LogIn.this, "Contraseña incorrecta", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        final Button privacyPolicyButton = findViewById(R.id.privacy_policy);
        privacyPolicyButton.setOnClickListener(v -> {
            Uri uri = Uri.parse("https://www.iubenda.com/privacy-policy/78355094");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        });
    }

    /**
     * Aquí se queda en la misma página cuando se dé al botón de atrás.
     */
    @Override
    public void onBackPressed() {
        return;
    }

    /**
     * Aquí se llama a la base de datos para obtener a los usuarios y poder comprobar los datos de acceso.
     */
    private void loadUsuarios() {
        usuariosRef.get().addOnSuccessListener(queryDocumentSnapshots -> {
            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                usuariosList.add(documentSnapshot.toObject(Usuario.class));
            }
        });
    }
}