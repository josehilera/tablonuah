package uah.gitlab.tablonuah.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LifecycleOwner;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Usuario;
import uah.gitlab.tablonuah.session.SessionConstants;
import uah.gitlab.tablonuah.utils.Links;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

/**
 * Actividad en la que un nuevo usuario puede registrarse rellenando los datos correspondientes.
 */
public class Registro extends AppCompatActivity {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference usuariosRef = db.collection("Usuario");

    EditText nombreUsuario;
    EditText password;
    EditText passwordRepetida;
    Button boton_register;
    TextView logIn;

    LifecycleOwner owner;
    Context context;

    SharedPreferences settings;

    List<Usuario> usuariosList = new ArrayList<Usuario>();

    /**
     * Método onCreate en el que se asocian los elementos de la UI y sus correspondientes acciones al interactuar con ellos.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        owner = this;
        context = this;

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        nombreUsuario = findViewById(R.id.edit_text_nombre);
        password = findViewById(R.id.edit_text_contrasenia);
        passwordRepetida = findViewById(R.id.edit_text_repetir_contrasenia);
        boton_register = findViewById(R.id.button_register);
        logIn = findViewById(R.id.inicia_sesion);

        //inicializarFirebase();

        loadUsuarios();

        SpannableString link = Links.crearSpan("Iniciar sesión", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LogIn.class);
                startActivity(intent);
                finish();
            }
        }, true, 0xFF1F13D3);

        logIn.append(" ");
        logIn.append(link);

        Links.makeLinksFocusable(logIn);

        boton_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean error = false;
                if (nombreUsuario.getText().toString().split(" ").length > 1) {
                    nombreUsuario.setError("El nombre de usuario no puede contener espacios");
                    error = true;
                }
                if (nombreUsuario.getText().toString().trim().equalsIgnoreCase("")) {
                    nombreUsuario.setError("Debes rellenar el nombre de usuario");
                    error = true;
                }
                if (nombreUsuario.getText().toString().length() > 15) {
                    nombreUsuario.setError("El nombre de usuario puede tener un máximo de 15 caracteres");
                    error = true;
                }
                if (password.getText().toString().equalsIgnoreCase("")) {
                    password.setError("Debes rellenar la contraseña");
                    error = true;
                }
                if (password.getText().toString().length() > 20) {
                    password.setError("La contraseña puede tener un máximo de 20 caracteres");
                    error = true;
                }
                if (passwordRepetida.getText().toString().equalsIgnoreCase("") || !passwordRepetida.getText().toString().equals(password.getText().toString())) {
                    passwordRepetida.setError("Las contraseñas deben coincidir");
                    error = true;
                }
                if (!error) {
                    Usuario usuarioIntroducido = null;
                    for (Usuario usuario : usuariosList) {
                        if (usuario.getNombreUsuario().equals(nombreUsuario.getText().toString())) {
                            usuarioIntroducido = usuario;
                        }
                    }
                    if (usuarioIntroducido != null) {
                        Toast.makeText(Registro.this, "Ya existe un usuario con ese nombre", Toast.LENGTH_LONG).show();
                    }
                    else {
                        settings.edit().putString(SessionConstants.USERNAME, nombreUsuario.getText().toString()).apply();

                        Usuario u = new Usuario(nombreUsuario.getText().toString(), password.getText().toString());

                        usuariosRef.document().set(u);

                        showRegisterDialog();
                    }
                }
            }
        });
    }

    private void showRegisterDialog() {
        ConstraintLayout registerConstraintLayout = findViewById(R.id.register_dialog);
        View view = LayoutInflater.from(Registro.this).inflate(R.layout.register_dialog, registerConstraintLayout);
        Button registerDone = view.findViewById(R.id.register_message_done);

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Registro.this);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();

        registerDone.findViewById(R.id.register_message_done).setOnClickListener(view1 -> {
            alertDialog.dismiss();
        });
        if (alertDialog.getWindow() != null) {
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Intent intent = new Intent(context, Inicio.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        alertDialog.show();
    }

    /*
    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        //firebaseDatabase.setPersistenceEnabled(true);

        databaseReference = firebaseDatabase.getReference();
    }*/

    /**
     * Aquí se llama a la base de datos para obtener a los usuarios y poder comprobar los datos de acceso.
     */
    private void loadUsuarios() {

        usuariosRef
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            usuariosList.add(documentSnapshot.toObject(Usuario.class));
                        }
                    }
                });

        /*
        databaseReference.child("Usuario").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                usuariosList.clear();
                for (DataSnapshot objSnapshot : snapshot.getChildren()) {
                    Usuario usuariosEntity = objSnapshot.getValue(Usuario.class);
                    usuariosList.add(usuariosEntity);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });*/
    }
}
