package uah.gitlab.tablonuah.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.OpenableColumns;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import uah.gitlab.tablonuah.R;
import uah.gitlab.tablonuah.model.Archivo;
import uah.gitlab.tablonuah.model.Asignatura;
import uah.gitlab.tablonuah.model.Curso;
import uah.gitlab.tablonuah.model.Facultad;
import uah.gitlab.tablonuah.model.Grado;
import uah.gitlab.tablonuah.model.NovedadArchivo;
import uah.gitlab.tablonuah.session.SessionConstants;

public class UploadFile extends AppCompatActivity {

    private static final int MENU_POSITION_UPLOAD = 2;

    private static int menuPosition = -1;

    private static final int PICK_IMAGE_REQUEST = 1;

    private StorageTask uploadTask;
    private Uri imageUri;

    private StorageReference storageReference;
    private CollectionReference archivoCollectionReference;
    private CollectionReference novedadArchivoCollectionReference;
    private CollectionReference facultadCollectionReference;
    private CollectionReference gradoCollectionReference;
    private CollectionReference cursoCollectionReference;
    private CollectionReference asignaturaCollectionReference;

    private ProgressBar progressBar;
    EditText file_name;
    Button upload;

    BottomNavigationView bottomNavigationView;

    SharedPreferences settings;

    private boolean camposIntroducidos = false;

    private Spinner spinner_facultad;
    private Spinner spinner_grado;
    private Spinner spinner_curso;
    private Spinner spinner_asignatura;
    private Spinner spinner_categoria;

    private ImageView imageView;

    List<String> listaFacultadesString = new ArrayList<>();
    List<String> listaGradosString = new ArrayList<>();
    List<String> listaCursosString = new ArrayList<>();
    List<String> listaAsignaturasString = new ArrayList<>();
    List<String> listaCategoriasString = new ArrayList<>();

    String nombreFacultadSeleccionada;
    String nombreGradoSeleccionado;
    String nombreCursoSeleccionado;
    String nombreAsignaturaSeleccionada;
    String nombreCategoriaSeleccionada = "";

    List<Facultad> facultadesList = new ArrayList<>();
    List<Grado> gradosList = new ArrayList<>();
    List<Curso> cursosList = new ArrayList<>();
    List<Asignatura> asignaturasList = new ArrayList<>();

    int idAsignaturaMarcada = -1;

    private Asignatura asignaturaMarcada;
    private Curso cursoMarcado;
    private Grado gradoMarcado;
    private Facultad facultadMarcada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_file);

        Intent intent = getIntent();
        menuPosition = intent.getIntExtra(SessionConstants.EXTRA_MENU_POSITION, -1);

        storageReference = FirebaseStorage.getInstance().getReference("Archivo");
        archivoCollectionReference = FirebaseFirestore.getInstance().collection("Archivo");
        novedadArchivoCollectionReference = FirebaseFirestore.getInstance().collection("NovedadArchivo");
        facultadCollectionReference = FirebaseFirestore.getInstance().collection("Facultad");
        gradoCollectionReference = FirebaseFirestore.getInstance().collection("Grado");
        cursoCollectionReference = FirebaseFirestore.getInstance().collection("Curso");
        asignaturaCollectionReference = FirebaseFirestore.getInstance().collection("Asignatura");

        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        bottomNavigationView = findViewById(R.id.bottom_navigation);

        imageView = findViewById(R.id.image_view);

        imageView.setVisibility(View.GONE);

        spinner_facultad = findViewById(R.id.spinner_facultad);
        spinner_grado = findViewById(R.id.spinner_grado);
        spinner_curso = findViewById(R.id.spinner_curso);
        spinner_asignatura = findViewById(R.id.spinner_asignatura);
        spinner_categoria = findViewById(R.id.spinner_categoria);

        Button upload_file = findViewById(R.id.examinar);
        progressBar = findViewById(R.id.progress_bar);

        progressBar.setVisibility(View.INVISIBLE);

        upload_file.setOnClickListener(v -> {
            filePicker();
        });

        upload = findViewById(R.id.upload);
        file_name = findViewById(R.id.filename);

        upload.setOnClickListener(view -> {
            if (uploadTask != null && uploadTask.isInProgress()) {
                Toast.makeText(UploadFile.this, "Subida en progreso", Toast.LENGTH_SHORT).show();
            } else if (nombreCategoriaSeleccionada.equals("")) {
                Toast.makeText(this, "Selecciona una asignatura y su categoría", Toast.LENGTH_LONG).show();
            } else if (file_name.getText().toString().isEmpty()) {
                Toast.makeText(this, "Debes asignar un nombre al archivo", Toast.LENGTH_LONG).show();
            }
            else {
                uploadFile();
            }
        });

        if (menuPosition == -1) {
            bottomNavigationView.getMenu().getItem(MENU_POSITION_UPLOAD).setChecked(true);
        }
        else {
            bottomNavigationView.getMenu().getItem(menuPosition).setChecked(true);
        }

        bottomNavigationView.setOnItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent intentHome = new Intent(UploadFile.this, Inicio.class);
                    intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    finish();
                    return true;
                case R.id.navigation_buscar:
                    Intent intentBuscador = new Intent(UploadFile.this, Buscador.class);
                    intentBuscador.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentBuscador);
                    finish();
                    return true;
                case R.id.navigation_documentos:
                    Intent intentApuntes = new Intent(UploadFile.this, Apuntes.class);
                    intentApuntes.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentApuntes);
                    finish();
                    return true;
                case R.id.navigation_subir:
                    if (camposIntroducidos || !file_name.getText().toString().isEmpty()) {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                        builder.setCancelable(true);
                        builder.setTitle("Actualizar pantalla");
                        builder.setMessage("¿Seguro que quieres recargar la página?\n\nSe borrarán los campos introducidos.");
                        builder.setPositiveButton("ACTUALIZAR",
                                (dialog, which) -> {
                                    loadUniversity();
                                    bottomNavigationView.getMenu().getItem(MENU_POSITION_UPLOAD).setChecked(true);
                                    camposIntroducidos = false;
                                });
                        builder.setNegativeButton("CANCELAR", (dialog, which) -> {
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                    return true;
                case R.id.navigation_perfil:
                    Intent intentPerfil = new Intent(UploadFile.this, DisplayProfile.class);
                    intentPerfil.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intentPerfil.putExtra(SessionConstants.EXTRA_ID, settings.getString(SessionConstants.USERNAME, "InvalidUser"));
                    startActivity(intentPerfil);
                    finish();
                    return true;
            }
            return false;
        });

        loadUniversity();
    }

    /**
     * Este método se encarga de llama a los otros métodos que cargan las facultades, grados, cursos, asignaturas y categorías para introducirlos en los spinners.
     */
    private void loadUniversity() {
        if (settings.getBoolean(SessionConstants.UPLOAD, false)) {
            obtenerDatosSubida();
        } else {
            spinner_grado.setEnabled(false);
            spinner_curso.setEnabled(false);
            spinner_asignatura.setEnabled(false);
            spinner_categoria.setEnabled(false);

            file_name.clearFocus();
            file_name.setText("");
            imageView.setVisibility(View.GONE);
            imageUri = null;

            loadFacultades();
            loadGrados();
            loadCursos();
            loadAsignaturas();
            loadCategorias();
        }
    }

    private void obtenerDatosSubida() {
        Intent intent = getIntent();
        nombreCategoriaSeleccionada = intent.getStringExtra(SessionConstants.EXTRA_CATEGORIA) != null ? intent.getStringExtra(SessionConstants.EXTRA_CATEGORIA) : "";
        idAsignaturaMarcada = intent.getIntExtra(SessionConstants.EXTRA_ID, -1);

        asignaturaCollectionReference.whereEqualTo("id", idAsignaturaMarcada).get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (!queryDocumentSnapshots.getDocuments().isEmpty()) {
                asignaturaMarcada = queryDocumentSnapshots.getDocuments().get(0).toObject(Asignatura.class);

                cursoCollectionReference.whereEqualTo("id", asignaturaMarcada.getIdCurso()).get().addOnSuccessListener(queryCursosDocumentSnapshots -> {
                    if (!queryCursosDocumentSnapshots.getDocuments().isEmpty()) {
                        cursoMarcado = queryCursosDocumentSnapshots.getDocuments().get(0).toObject(Curso.class);

                        gradoCollectionReference.whereEqualTo("id", cursoMarcado.getIdGrado()).get().addOnSuccessListener(queryGradosDocumentSnapshots -> {
                            if (!queryGradosDocumentSnapshots.getDocuments().isEmpty()) {
                                gradoMarcado = queryGradosDocumentSnapshots.getDocuments().get(0).toObject(Grado.class);

                                facultadCollectionReference.whereEqualTo("id", gradoMarcado.getIdFacultad()).get().addOnSuccessListener(queryFacultadesDocumentSnapshots -> {
                                    if (!queryFacultadesDocumentSnapshots.getDocuments().isEmpty()) {
                                        facultadMarcada = queryFacultadesDocumentSnapshots.getDocuments().get(0).toObject(Facultad.class);

                                        loadSpecificFacultad();
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    private void loadSpecificFacultad() {
        facultadCollectionReference.orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                facultadesList.add(documentSnapshot.toObject(Facultad.class));
            }
            listaFacultadesString.clear();

            listaFacultadesString.add("Elija una facultad");

            for(Facultad facultad : facultadesList) {
                listaFacultadesString.add(facultad.getNombre());
            }

            ArrayAdapter<String> adapterFacultad = new ArrayAdapter<String>(this,
                    R.layout.custom_simple_spinner_item, listaFacultadesString){
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View mView =  super.getDropDownView(position, convertView, parent);
                    TextView mTextView = (TextView) mView;
                    if(position == 0) {
                        mTextView.setTextColor(Color.GRAY);
                    }
                    return mView;
                }
            };
            adapterFacultad.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

            spinner_facultad.setAdapter(adapterFacultad);

            boolean facultadFound = false;
            for (int i = 0; !facultadFound && i < listaFacultadesString.size(); i++) {
                if (listaFacultadesString.get(i).equals(facultadMarcada.getNombre())) {
                    spinner_facultad.setSelection(i);
                    facultadFound = true;
                }
            }

            spinner_facultad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        camposIntroducidos = true;

                        nombreFacultadSeleccionada = (String) parent.getSelectedItem();

                        spinner_grado.setEnabled(true);

                        if (!settings.getBoolean(SessionConstants.UPLOAD, false)) {
                            idAsignaturaMarcada = -1;

                            nombreCategoriaSeleccionada = "";

                            listaCursosString.clear();
                            listaCursosString.add("Elija un curso");
                            spinner_curso.setSelection(0);
                            spinner_curso.setEnabled(false);

                            listaAsignaturasString.clear();
                            listaAsignaturasString.add("Elija una asignatura");
                            spinner_asignatura.setSelection(0);
                            spinner_asignatura.setEnabled(false);

                            listaCategoriasString.clear();
                            listaCategoriasString.add("Elija una categoría");
                            spinner_categoria.setSelection(0);
                            spinner_categoria.setEnabled(false);

                            gradoCollectionReference.whereEqualTo("idFacultad", facultadesList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                                gradosList.clear();
                                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                    gradosList.add(documentSnapshot.toObject(Grado.class));
                                }
                                listaGradosString.clear();

                                listaGradosString.add("Elija un grado");

                                for(Grado grado : gradosList) {
                                    listaGradosString.add(grado.getNombre());
                                }

                                spinner_grado.setSelection(0);
                            });
                        } else {
                            loadSpecificGrado();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });
    }

    private void loadSpecificGrado() {
        spinner_grado.setEnabled(true);

        gradoCollectionReference.whereEqualTo("idFacultad", facultadMarcada.getId()).orderBy("id").get().addOnSuccessListener(queryGradosFacultadDocumentSnapshots -> {
            gradosList.clear();
            gradosList = queryGradosFacultadDocumentSnapshots.toObjects(Grado.class);

            listaGradosString.clear();

            listaGradosString.add("Elija una asignatura");

            for (Grado grado : gradosList) {
                listaGradosString.add(grado.getNombre());
            }

            ArrayAdapter<String> adapterGrado = new ArrayAdapter<String>(this,
                    R.layout.custom_simple_spinner_item, listaGradosString){
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View mView =  super.getDropDownView(position, convertView, parent);
                    TextView mTextView = (TextView) mView;
                    if(position == 0) {
                        mTextView.setTextColor(Color.GRAY);
                    }
                    return mView;
                }
            };
            adapterGrado.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

            spinner_grado.setAdapter(adapterGrado);

            boolean gradoFound = false;
            for (int i = 0; !gradoFound && i < listaGradosString.size(); i++) {
                if (listaGradosString.get(i).equals(gradoMarcado.getNombre())) {
                    spinner_grado.setSelection(i);
                    gradoFound = true;
                }
            }

            spinner_grado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        nombreGradoSeleccionado = (String) parent.getSelectedItem();

                        spinner_curso.setEnabled(true);

                        if (!settings.getBoolean(SessionConstants.UPLOAD, false)) {
                            idAsignaturaMarcada = -1;

                            nombreCategoriaSeleccionada = "";

                            listaAsignaturasString.clear();
                            listaAsignaturasString.add("Elija una asignatura");
                            spinner_asignatura.setSelection(0);
                            spinner_asignatura.setEnabled(false);

                            listaCategoriasString.clear();
                            listaCategoriasString.add("Elija una categoría");
                            spinner_categoria.setSelection(0);
                            spinner_categoria.setEnabled(false);

                            cursoCollectionReference.whereEqualTo("idGrado", gradosList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                                cursosList.clear();
                                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                    cursosList.add(documentSnapshot.toObject(Curso.class));
                                }

                                listaCursosString.clear();

                                listaCursosString.add("Elija un curso");

                                for (Curso curso : cursosList) {
                                    listaCursosString.add(curso.getNombre());
                                }

                                spinner_curso.setSelection(0);
                            });
                        } else {
                            loadSpecificCurso();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });
    }

    private void loadSpecificCurso() {
        spinner_curso.setEnabled(true);

        cursoCollectionReference.whereEqualTo("idGrado", gradoMarcado.getId()).orderBy("id").get().addOnSuccessListener(queryCursosGradoDocumentSnapshots -> {
            cursosList.clear();
            cursosList = queryCursosGradoDocumentSnapshots.toObjects(Curso.class);

            listaCursosString.clear();

            listaCursosString.add("Elija una asignatura");

            for (Curso curso : cursosList) {
                listaCursosString.add(curso.getNombre());
            }

            ArrayAdapter<String> adapterCurso = new ArrayAdapter<String>(this,
                    R.layout.custom_simple_spinner_item, listaCursosString){
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View mView =  super.getDropDownView(position, convertView, parent);
                    TextView mTextView = (TextView) mView;
                    if(position == 0) {
                        mTextView.setTextColor(Color.GRAY);
                    }
                    return mView;
                }
            };
            adapterCurso.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

            spinner_curso.setAdapter(adapterCurso);

            boolean cursoFound = false;
            for (int i = 0; !cursoFound && i < listaCursosString.size(); i++) {
                if (listaCursosString.get(i).equals(cursoMarcado.getNombre())) {
                    spinner_curso.setSelection(i);
                    cursoFound = true;
                }
            }

            spinner_curso.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        nombreCursoSeleccionado = (String) parent.getSelectedItem();

                        spinner_asignatura.setEnabled(true);

                        if (!settings.getBoolean(SessionConstants.UPLOAD, false)) {
                            idAsignaturaMarcada = -1;

                            nombreCategoriaSeleccionada = "";

                            listaCategoriasString.clear();
                            listaCategoriasString.add("Elija una categoría");
                            spinner_categoria.setSelection(0);
                            spinner_categoria.setEnabled(false);

                            asignaturaCollectionReference.whereEqualTo("idCurso", cursosList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                                asignaturasList.clear();
                                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                    asignaturasList.add(documentSnapshot.toObject(Asignatura.class));
                                }
                                listaAsignaturasString.clear();

                                listaAsignaturasString.add("Elija una asignatura");

                                for(Asignatura asignatura : asignaturasList) {
                                    listaAsignaturasString.add(asignatura.getNombre());
                                }

                                spinner_asignatura.setSelection(0);
                            });
                        } else {
                            loadSpecificAsignatura();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });
    }

    private void loadSpecificAsignatura() {
        spinner_asignatura.setEnabled(true);

        asignaturaCollectionReference.whereEqualTo("idCurso", cursoMarcado.getId()).orderBy("id").get().addOnSuccessListener(queryAsignaturasCursoDocumentSnapshots -> {
            asignaturasList.clear();
            asignaturasList = queryAsignaturasCursoDocumentSnapshots.toObjects(Asignatura.class);

            listaAsignaturasString.clear();

            listaAsignaturasString.add("Elija una asignatura");

            for (Asignatura asignatura : asignaturasList) {
                listaAsignaturasString.add(asignatura.getNombre());
            }

            ArrayAdapter<String> adapterAsignatura = new ArrayAdapter<String>(this,
                    R.layout.custom_simple_spinner_item, listaAsignaturasString){
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View mView =  super.getDropDownView(position, convertView, parent);
                    TextView mTextView = (TextView) mView;
                    if(position == 0) {
                        mTextView.setTextColor(Color.GRAY);
                    }
                    return mView;
                }
            };
            adapterAsignatura.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

            spinner_asignatura.setAdapter(adapterAsignatura);

            boolean asignaturaFound = false;
            for (int i = 0; !asignaturaFound && i < listaAsignaturasString.size(); i++) {
                if (listaAsignaturasString.get(i).equals(asignaturaMarcada.getNombre())) {
                    spinner_asignatura.setSelection(i);
                    asignaturaFound = true;
                }
            }

            spinner_asignatura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0) {
                        nombreAsignaturaSeleccionada = (String) parent.getSelectedItem();

                        spinner_categoria.setEnabled(true);

                        if (!settings.getBoolean(SessionConstants.UPLOAD, false)) {
                            nombreCategoriaSeleccionada = "";

                            for (Asignatura a : asignaturasList) {
                                if (a.getNombre().equals(nombreAsignaturaSeleccionada)) {
                                    idAsignaturaMarcada = a.getId();
                                }
                            }

                            listaCategoriasString.clear();

                            listaCategoriasString.add("Elija una categoría");

                            listaCategoriasString.add("Teoría");
                            listaCategoriasString.add("Ejercicios");
                            listaCategoriasString.add("Prácticas");
                            listaCategoriasString.add("Exámenes");

                            spinner_categoria.setSelection(0);
                        } else {
                            loadSpecificCategoria();
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });
    }

    private void loadSpecificCategoria() {
        settings.edit().putBoolean(SessionConstants.UPLOAD, false).apply();

        listaCategoriasString = new ArrayList<String>();

        listaCategoriasString.add("Elija una categoría");

        spinner_categoria.setEnabled(true);

        listaCategoriasString.add("Teoría");
        listaCategoriasString.add("Ejercicios");
        listaCategoriasString.add("Prácticas");
        listaCategoriasString.add("Exámenes");

        ArrayAdapter<String> adapterCategoria = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaCategoriasString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterCategoria.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_categoria.setAdapter(adapterCategoria);

        boolean categoriaFound = false;
        for (int i = 0; !categoriaFound && i < listaCategoriasString.size(); i++) {
            if (listaCategoriasString.get(i).equals(nombreCategoriaSeleccionada)) {
                spinner_categoria.setSelection(i);
                categoriaFound = true;
            }
        }

        spinner_categoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreCategoriaSeleccionada = (String) parent.getSelectedItem();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Aquí se inicializa el spinner de las facultades y se cargan los grados correspondientes a la facultad seleccionada.
     */
    private void loadFacultades() {
        listaFacultadesString = new ArrayList<String>();

        listaFacultadesString.add("Elija una facultad");

        ArrayAdapter<String> adapterFacultad = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaFacultadesString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterFacultad.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_facultad.setAdapter(adapterFacultad);

        spinner_facultad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    camposIntroducidos = true;

                    nombreFacultadSeleccionada = (String) parent.getSelectedItem();

                    spinner_grado.setEnabled(true);

                    idAsignaturaMarcada = -1;

                    nombreCategoriaSeleccionada = "";

                    listaCursosString.clear();
                    listaCursosString.add("Elija un curso");
                    spinner_curso.setSelection(0);
                    spinner_curso.setEnabled(false);

                    listaAsignaturasString.clear();
                    listaAsignaturasString.add("Elija una asignatura");
                    spinner_asignatura.setSelection(0);
                    spinner_asignatura.setEnabled(false);

                    listaCategoriasString.clear();
                    listaCategoriasString.add("Elija una categoría");
                    spinner_categoria.setSelection(0);
                    spinner_categoria.setEnabled(false);

                    gradoCollectionReference.whereEqualTo("idFacultad", facultadesList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                        gradosList.clear();
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            gradosList.add(documentSnapshot.toObject(Grado.class));
                        }
                        listaGradosString.clear();

                        listaGradosString.add("Elija un grado");

                        for(Grado grado : gradosList) {
                            listaGradosString.add(grado.getNombre());
                        }

                        spinner_grado.setSelection(0);
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        facultadCollectionReference.orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
            facultadesList.clear();
            for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                facultadesList.add(documentSnapshot.toObject(Facultad.class));
            }
            listaFacultadesString.clear();

            listaFacultadesString.add("Elija una facultad");

            for(Facultad facultad : facultadesList) {
                listaFacultadesString.add(facultad.getNombre());
            }
        });
    }


    /**
     * Aquí se inicializa el spinner de los grados y se cargan los cursos correspondientes al grado seleccionado.
     */
    private void loadGrados() {
        listaGradosString = new ArrayList<String>();

        listaGradosString.add("Elija un grado");

        ArrayAdapter<String> adapterGrado = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaGradosString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterGrado.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_grado.setAdapter(adapterGrado);

        spinner_grado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreGradoSeleccionado = (String) parent.getSelectedItem();

                    spinner_curso.setEnabled(true);

                    idAsignaturaMarcada = -1;

                    nombreCategoriaSeleccionada = "";

                    listaAsignaturasString.clear();
                    listaAsignaturasString.add("Elija una asignatura");
                    spinner_asignatura.setSelection(0);
                    spinner_asignatura.setEnabled(false);

                    listaCategoriasString.clear();
                    listaCategoriasString.add("Elija una categoría");
                    spinner_categoria.setSelection(0);
                    spinner_categoria.setEnabled(false);

                    cursoCollectionReference.whereEqualTo("idGrado", gradosList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                        cursosList.clear();
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            cursosList.add(documentSnapshot.toObject(Curso.class));
                        }

                        listaCursosString.clear();

                        listaCursosString.add("Elija un curso");

                        for (Curso curso : cursosList) {
                            listaCursosString.add(curso.getNombre());
                        }

                        spinner_curso.setSelection(0);
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Aquí se inicializa el spinner de los cursos y se cargan las asignaturas correspondientes al curso seleccionado.
     */
    private void loadCursos() {
        listaCursosString = new ArrayList<String>();

        listaCursosString.add("Elija un curso");

        ArrayAdapter<String> adapterCurso = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaCursosString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterCurso.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_curso.setAdapter(adapterCurso);

        spinner_curso.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreCursoSeleccionado = (String) parent.getSelectedItem();

                    spinner_asignatura.setEnabled(true);

                    idAsignaturaMarcada = -1;

                    nombreCategoriaSeleccionada = "";

                    listaCategoriasString.clear();
                    listaCategoriasString.add("Elija una categoría");
                    spinner_categoria.setSelection(0);
                    spinner_categoria.setEnabled(false);

                    asignaturaCollectionReference.whereEqualTo("idCurso", cursosList.get(position - 1).getId()).orderBy("id").get().addOnSuccessListener(queryDocumentSnapshots -> {
                        asignaturasList.clear();
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            asignaturasList.add(documentSnapshot.toObject(Asignatura.class));
                        }
                        listaAsignaturasString.clear();

                        listaAsignaturasString.add("Elija una asignatura");

                        for(Asignatura asignatura : asignaturasList) {
                            listaAsignaturasString.add(asignatura.getNombre());
                        }

                        spinner_asignatura.setSelection(0);
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    /**
     * Aquí se inicializa el spinner de las asignaturas, se guarda el id de la que el usuario seleccione y se cargan las cuatro categorías en su correspondiente spinner.
     */
    private void loadAsignaturas() {
        listaAsignaturasString = new ArrayList<String>();

        listaAsignaturasString.add("Elija una asignatura");

        ArrayAdapter<String> adapterAsignatura = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaAsignaturasString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterAsignatura.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_asignatura.setAdapter(adapterAsignatura);

        spinner_asignatura.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreAsignaturaSeleccionada = (String) parent.getSelectedItem();

                    spinner_categoria.setEnabled(true);

                    nombreCategoriaSeleccionada = "";

                    for (Asignatura a : asignaturasList) {
                        if (a.getNombre().equals(nombreAsignaturaSeleccionada)) {
                            idAsignaturaMarcada = a.getId();
                        }
                    }

                    listaCategoriasString.clear();

                    listaCategoriasString.add("Elija una categoría");

                    listaCategoriasString.add("Teoría");
                    listaCategoriasString.add("Ejercicios");
                    listaCategoriasString.add("Prácticas");
                    listaCategoriasString.add("Exámenes");

                    spinner_categoria.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Aquí se inicializa el spinner de las categorías y se guarda el nombre de la que el usuario seleccione.
     */
    private void loadCategorias() {
        listaCategoriasString = new ArrayList<String>();

        listaCategoriasString.add("Elija una categoría");

        ArrayAdapter<String> adapterCategoria = new ArrayAdapter<String>(this,
                R.layout.custom_simple_spinner_item, listaCategoriasString){
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View mView =  super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;
                if(position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                }
                return mView;
            }
        };
        adapterCategoria.setDropDownViewResource(R.layout.custom_simple_spinner_dropdown_item);

        spinner_categoria.setAdapter(adapterCategoria);

        spinner_categoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    nombreCategoriaSeleccionada = (String) parent.getSelectedItem();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Este método se utiliza para poder seleccionar el archivo a subir. Actualmente solo puede ser una imagen.
     */
    private void filePicker() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void uploadFile() {
        if (imageUri != null) {
            progressBar.setVisibility(View.VISIBLE);
            int idArchivo = (int) System.currentTimeMillis();
            StorageReference fileReference = storageReference.child(idArchivo + "." + getFileExtension(imageUri));
            uploadTask = fileReference.putFile(imageUri).addOnSuccessListener(taskSnapshot -> {
                file_name.clearFocus();

                Handler handler = new Handler();
                handler.postDelayed(() -> progressBar.setProgress(0), 500);

                Toast.makeText(UploadFile.this, "Archivo subido correctamente", Toast.LENGTH_SHORT).show();

                Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                while (!urlTask.isSuccessful());
                //Uri downloadUrl = urlTask.getResult();

                //Log.d(TAG, "onSuccess: firebase download url: " + downloadUrl.toString()); //use if testing...don't need this line.

                java.util.Date date = new java.util.Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Archivo archivo = new Archivo(idArchivo,
                                    file_name.getText().toString(),
                                    nombreCategoriaSeleccionada,
                                    idAsignaturaMarcada,
                                    settings.getString(SessionConstants.USERNAME, "InvalidUser"),
                                    (int) taskSnapshot.getTotalByteCount(),
                                    formatter.format(date),
                                    getFileExtension(imageUri));

                archivoCollectionReference.document(archivo.getId() + "").set(archivo);

                NovedadArchivo novedadArchivo = new NovedadArchivo(formatter.format(date), settings.getString(SessionConstants.USERNAME, "InvalidUser"), idArchivo, idAsignaturaMarcada);

                novedadArchivoCollectionReference.document().set(novedadArchivo);

                Intent intent = new Intent(UploadFile.this, DisplayArchivo.class);
                intent.putExtra(SessionConstants.EXTRA_MENU_POSITION, menuPosition != -1 ? menuPosition : MENU_POSITION_UPLOAD);
                intent.putExtra(SessionConstants.EXTRA_ID, idArchivo);
                startActivity(intent);
            }).addOnFailureListener(e -> Toast.makeText(UploadFile.this, e.getMessage(), Toast.LENGTH_SHORT).show())
            .addOnProgressListener(snapshot -> {
                double progress = (100.0 * snapshot.getBytesTransferred() / snapshot.getTotalByteCount());
                progressBar.setProgress((int) progress);
            });
        } else {
            Toast.makeText(this, "Selecciona un archivo", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            imageUri = data.getData();

            if (file_name.getText().toString().isEmpty()) {
                file_name.setText(getFileName(imageUri));
            }
            file_name.clearFocus();

            imageView.setVisibility(View.VISIBLE);
            Picasso.get().load(imageUri).into(imageView);

            camposIntroducidos = true;
        }
    }

    @SuppressLint("Range")
    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (menuPosition != -1) {
            super.onBackPressed();
        }
    }

}