package uah.gitlab.tablonuah.utils;

import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

/**
 * Clase utilizada para crear los links para acceder a archivos o perfiles de usuario.
 */
public class Links {

    public static SpannableString crearSpan(CharSequence texto, View.OnClickListener listener, boolean subrayado, int color) {
        SpannableString link = new SpannableString(texto);
        link.setSpan(new MyClickableString(listener, subrayado, color), 0, texto.length(),
                SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        return link;
    }

    private static class MyClickableString extends ClickableString{ // extend ClickableSpan

        boolean subrayado;

        int color;

        public MyClickableString(View.OnClickListener listener, boolean subrayado, int color) {
            super(listener);
            this.subrayado = subrayado;
            this.color = color;
        }

        @Override
        public void updateDrawState(@NonNull TextPaint ds) {
            ds.setColor(color);
            ds.setUnderlineText(subrayado);
        }
    }

    public static void makeLinksFocusable(TextView tv) {
        MovementMethod m = tv.getMovementMethod();
        if ((m == null) || !(m instanceof LinkMovementMethod)) {
            if (tv.getLinksClickable()) {
                tv.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

    private static class ClickableString extends ClickableSpan {
        private View.OnClickListener mListener;
        public ClickableString(View.OnClickListener listener) {
            mListener = listener;
        }
        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }
    }
}
