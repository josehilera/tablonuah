### Contenido del Dockerfile:
- Mismo Dockerfile que fabernovel/android:api-33-v1.7.0 sin incluir de la línea 97 (## Install Android SDK) hacia abajo
- Comandos para instalar dependencias necesitadas para poder usar fastlane frameit
- Scripts de before_script de illuzor menos la última línea (chmod +x ./gradlew)
- Scripts de instrumented tests de illuzor menos las partes de "# download script for emulator waiting" y "# run emulator and tests"